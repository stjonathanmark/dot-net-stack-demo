﻿
namespace OnlineShop
{
    public class BaseCard
    {
        public string Type { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }

        public int? ExpirationMonth { get; set; }

        public int? ExpirationYear { get; set; }

        public string Cvc { get; set; }
    }
}
