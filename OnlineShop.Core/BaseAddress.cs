﻿

namespace OnlineShop
{
    public enum AddressType
    {
        Residence = 0,
        Business = 1,
        Work = 2,
        School = 3,
        Governmental = 4
    }

    public class BaseAddress : BaseEntity<long>
    {
        public long? CustomerProfileId { get; set; }

        public long? OrderId { get; set; }

        public long? PaymentMethodId { get; set; }

        public int StateId { get; set; }

        public int CountryId { get; set; }

        public bool IsPrimary { get; set; }

        public AddressType Type { get; set; }

        public string Name { get; set; }

        public string StreetOne { get; set; }

        public string StreetTwo { get; set; }

        public string City { get; set; }

        public string ZipCode { get; set; }
    }
}
