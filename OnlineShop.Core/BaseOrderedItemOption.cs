﻿
namespace OnlineShop
{
    public class BaseOrderedItemOption
    {
        public long OrderedItemId { get; set; }

        public int OptionId { get; set; }
    }
}
