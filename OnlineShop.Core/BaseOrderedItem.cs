﻿
namespace OnlineShop
{
    public class BaseOrderedItem : BaseEntity<long>
    {
        public long OrderId { get; set; }

        public int ProductId { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }
    }
}
