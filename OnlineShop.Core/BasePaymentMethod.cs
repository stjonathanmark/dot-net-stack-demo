﻿using System;

namespace OnlineShop
{
    public class BasePaymentMethod : BaseEntity<long>
    {
        public long CustomerProfileId { get; set; }

        public DateTime CreationDate { get; set; }
        
        public string CustomerId { get; set; }

        public string SourceCardId { get; set; }

        public string CardType { get; set; }

        public DateTime ExpirationDate { get; set; }

        public string LastFourDigits { get; set; }
    }
}
