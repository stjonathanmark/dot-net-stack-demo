﻿
namespace OnlineShop
{
    public class BaseUserRole
    {
        public long UserId { get; set; }

        public int RoleId { get; set; }
    }
}
