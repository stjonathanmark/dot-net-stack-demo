﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShop
{
    public class BaseState : BaseEntity
    {
        public int CountryId { get; set; }

        public string Abbreviation { get; set; }

        public string Name { get; set; }
    }
}
