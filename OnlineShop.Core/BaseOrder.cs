﻿using System;

namespace OnlineShop
{
    public enum OrderStatus
    {
        Pending = 0,
        Paid = 1,
        Processing = 2,
        Processed = 3,
        PartiallyShipped = 4,
        Shipped = 5,
        Cancelled = 6,
        Declined = 7,
        PartiallyRefunded = 8,
        Refunded = 9,
        Disputed = 10,
        Verified = 11
    }

    public class BaseOrder : BaseEntity<long>
    {
        public long CustomerProfileId { get; set; }

        public long PaymentMethodId { get; set; }

        public DateTime Date { get; set; }

        public OrderStatus Status { get; set; }

        public decimal SubTotal { get; set; }

        public decimal Tax { get; set; }

        public decimal ShippingCost { get; set; }

        public decimal Total { get; set; }

        public string PaymentId { get; set; }
    } 
}
