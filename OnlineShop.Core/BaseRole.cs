﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShop
{
    public class BaseRole : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
