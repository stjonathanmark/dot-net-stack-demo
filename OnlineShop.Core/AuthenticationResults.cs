﻿
namespace OnlineShop
{
    public enum AuthenticationResult
    {
        Successful = 0,
        UserDoesNotExist = 1,
        InvalidPassword = 2,
        Inactive = 3,
        Disabled = 4,
        PasswordExpired = 5,
        LockedOut = 6
    }

    public enum PasswordChangeResult
    {
        Successful = 0,
        InvalidPasswordFormat = 1,
        PasswordReused = 2
    }

    public enum AccountCreationResult
    {
        Successful = 0,
        UsernameAlreadyExists = 1,
        InvalidUsernameFormat = 2,
        InvalidPasswordFormat = 3
    }
}
