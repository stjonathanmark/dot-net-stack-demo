﻿
namespace OnlineShop
{
    public enum EmailAddressTypes
    {
        Personal = 0,
        Business = 1,
        Work = 2,
        School = 3,
        Governmental = 4
    }

    public class BaseEmailAddress : BaseEntity<long>
    {
        public long? UserId { get; set; }

        public long? CustomerProfileId { get; set; }

        public bool IsPrimary { get; set; }

        public EmailAddressTypes Type { get; set; }

        public string Address { get; set; }
    }
}
