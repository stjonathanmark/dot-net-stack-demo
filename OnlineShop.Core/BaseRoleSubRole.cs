﻿
namespace OnlineShop
{
    public class BaseRoleSubRole
    {
        public int RoleId { get; set; }

        public int SubRoleId { get; set; }
    }
}
