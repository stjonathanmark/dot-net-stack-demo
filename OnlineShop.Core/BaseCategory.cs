﻿
namespace OnlineShop
{
    public class BaseCategory : BaseEntity
    {
        public int? ParentCategoryId { get; set; }

        public  string Name { get; set; }

        public string Description { get; set; }
    }
}
