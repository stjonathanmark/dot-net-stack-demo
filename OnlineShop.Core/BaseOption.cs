﻿
namespace OnlineShop
{
    public class BaseOption : BaseEntity
    {
        public int OptionSetId { get; set; }

        public int SortOrder { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}
