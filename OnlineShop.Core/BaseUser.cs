﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShop
{
    public class BaseUser : BaseEntity<long>
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public bool Activated { get; set; }

        public bool Enabled { get; set; }

        public bool LockedOut { get; set; }

        public bool PasswordIsTemporary { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime LastPasswordChangeDate { get; set; }

        public int FailedLoginAttempts { get; set; }

        public string Salt { get; set; }
    }
}
