﻿
using System;

namespace OnlineShop
{
    public class BasePayment
    {
        public string Id { get; set; }

        public DateTime PaymentDate { get; set; }

        public string Status { get; set; }

        public virtual bool Succeeded { get; set; }

        public virtual bool Pending { get; set; }

        public virtual bool Failed { get; set; }

        public string FailureCode { get; set; }

        public string FailureMessage { get; set; }

        public bool Paid { get; set; }

        public bool? Captured { get; set; }

        public int AmountInCents { get; set; }

        public virtual decimal Amount { get; set; }

    }
}
