﻿
namespace OnlineShop
{
    public enum PhoneNumberType
    {
        Home = 0,
        Mobile = 1,
        Work = 2,
        Business = 3,
        School = 4,
        Governmental = 5
    }

    public class BasePhoneNumber : BaseEntity<long>
    {
        public long CustomerProfileId { get; set; }

        public bool IsPrimary { get; set; }

        public PhoneNumberType Type { get; set; }

        public string Number { get; set; }

        public string Extension { get; set; }
    }
}
