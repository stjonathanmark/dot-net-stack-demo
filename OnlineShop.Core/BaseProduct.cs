﻿
namespace OnlineShop
{
    public class BaseProduct : BaseEntity
    {
        public int CategoryId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public decimal Weight { get; set; }

        public bool InStock { get; set; }

        public string ImageUrl { get; set; }

        public string ThumbnailImageUrl { get; set; }
    }
}
