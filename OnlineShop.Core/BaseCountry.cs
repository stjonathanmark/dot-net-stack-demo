﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShop
{
    public class BaseCountry : BaseEntity
    {
        public string Abbreviation { get; set; }

        public string Name { get; set; }
    }
}
