﻿
using System;

namespace OnlineShop
{
    public class BasePassword : BaseEntity<long>
    {
        public long UserId { get; set; }

        public DateTime CreationDate { get; set; } 

        public bool Temporary { get; set; }

        public string Secret { get; set; }

        public string Salt { get; set; }
    }
}
