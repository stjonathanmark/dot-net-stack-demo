﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShop
{
    public class BasePaymentToken
    {
        public string TokenId { get; set; }

        public string CreateTokenId { get; set; }
    }
}
