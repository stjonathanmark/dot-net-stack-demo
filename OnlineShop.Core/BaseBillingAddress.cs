﻿
namespace OnlineShop
{
    public class BaseBillingAddress
    {
        public string StreetOne { get; set; }

        public string StreetTwo { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        public string ZipCode { get; set; }
    }
}
