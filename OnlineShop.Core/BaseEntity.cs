﻿
namespace OnlineShop
{
    public class BaseEntity<TEntityId>
        where TEntityId : struct
    {
        public TEntityId Id { get; set; }

        public bool Deleted { get; set; }
    }

    public class BaseEntity : BaseEntity<int>
    {

    }
}
