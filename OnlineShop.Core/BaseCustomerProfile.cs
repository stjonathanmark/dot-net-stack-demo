﻿
using System;

namespace OnlineShop
{
    public class BaseCustomerProfile : BaseEntity<long>
    {
        public  long UserId { get; set; }

        public bool IsPersonal { get; set; }

        public string CompanyName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PaymentGatewayId { get; set; }

        public DateTime? BirthDate { get; set; }
    }
}
