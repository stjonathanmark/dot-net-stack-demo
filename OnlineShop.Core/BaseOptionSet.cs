﻿
namespace OnlineShop
{
    public class BaseOptionSet : BaseEntity
    {
        public int? ProductId { get; set; }

        public string Name { get; set; }
    }
}
