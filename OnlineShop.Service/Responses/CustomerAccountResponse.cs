﻿using OnlineShop.Dto;

namespace OnlineShop.Service
{
    public class CustomerAccountResponse : Response
    {
        public bool Authenticated { get; set; }

        public UserDto User { get; set; }
    }
}
