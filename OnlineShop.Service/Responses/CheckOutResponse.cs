﻿using System.Collections.Generic;
using OnlineShop.Dto;

namespace OnlineShop.Service
{
    public class CheckOutResponse : Response
    {
        public OrderStatus OrderStatus { get; set; }

        public long OrderId { get; set; }

        public OrderDto Order { get; set; }

        public bool PaymentApproved { get; set; }

        public string PaymentMessage { get; set; }

        public IEnumerable<StateDto> States { get; set; }

        public PaymentTokenDto PaymentToken { get; set; }
    }
}
