﻿using System.Collections.Generic;
using OnlineShop.Dto;

namespace OnlineShop.Service
{
    public class SecurityResponse : Response
    {
        public SecurityResponse()
            : base()
        {

        }

        public SecurityResponse(Response response)
            : base(response)
        {

        }

        public AuthenticationResult AuthenticationResult { get; set; }

        public string AuthenticationMessage { get; set; }

        public string AccountCreationMessage { get; set; }

        public AccountCreationResult AccountCreationResult { get; set; }

        public long UserId { get; set; }

        public TokenUserDto TokenUser { get; set; }

        public UserDto User { get; set; }

        public IEnumerable<UserDto> Users { get; set; }

        public bool Authenticated { get; set; }

        public bool AccountCreated { get; set; }
    }
}
