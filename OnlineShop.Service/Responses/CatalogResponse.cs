﻿using System.Collections.Generic;
using OnlineShop.Dto;

namespace OnlineShop.Service
{
    public class CatalogResponse : Response
    {
        public IEnumerable<CategoryDto> Categories { get; set; }

        public IEnumerable<ProductDto> Products { get; set; }

        public CategoryDto Category { get; set; }

        public ProductDto Product { get; set; }
    }
}
