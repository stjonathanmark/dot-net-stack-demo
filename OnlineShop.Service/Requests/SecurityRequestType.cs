﻿
namespace OnlineShop.Service
{
    public enum SecurityRequestType
    {
        CreateUserAccount,
        AuthenticateUser,
        GetUserForTokenCreation
    }
}
