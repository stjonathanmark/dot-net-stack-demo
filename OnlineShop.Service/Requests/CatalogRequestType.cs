﻿
namespace OnlineShop.Service
{
    public enum CatalogRequestType
    {
        GetAllCategories,
        GetSubCategories,
        GetAllProducts,
        GetProductsByCategory,
        SearchProducts,
        GetCategoryByProduct,
        GetProductDetails,
        GetCategory
    }
}
