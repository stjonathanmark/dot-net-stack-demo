﻿
namespace OnlineShop.Service
{
    public enum CustomerAccountRequestType
    {
        Login,
        Logout,
        ChangePassword,
        CreateAccount,
        ActivateAccount,
        DeactivateAcount,
        EnableAccount,
        DisableAccount,
        GetProfileOverview,
        GetPersonalInfo,
        GetPastOrders,
        SearchPastOrder,
        GetOrderDetails,
        GetPaymentMethods,
        GetPaymentMethodDetails,
        UpdatePaymentMethod,
        RemovePaymentMethod
    }
}
