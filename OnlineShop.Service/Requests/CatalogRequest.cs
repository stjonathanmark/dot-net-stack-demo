﻿
namespace OnlineShop.Service
{
    public class CatalogRequest : Request
    {
        public CatalogRequest()
            : base()
        {

        }

        public CatalogRequest(CatalogRequestType requestType)
            : this()
        {
            Type = requestType;
        }

        public CatalogRequestType Type { get; set; }

        public int? ProductId { get; set; }

        public int? CategoryId { get; set; }

        public string SearchTerm { get; set; }
    }
}
