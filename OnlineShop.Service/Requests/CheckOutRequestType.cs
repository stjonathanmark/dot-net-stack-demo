﻿
namespace OnlineShop.Service
{
    public enum CheckOutRequestType
    {
        GetShippingOptions,
        GetPaymentOptions,
        GetPaymentToken,
        MakePayment
    }
}
