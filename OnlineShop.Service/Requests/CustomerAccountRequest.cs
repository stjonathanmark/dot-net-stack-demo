﻿
namespace OnlineShop.Service
{
    public class CustomerAccountRequest : Request
    {
        public CustomerAccountRequest()
            : base()
        {

        }

        public CustomerAccountRequest(CustomerAccountRequestType requestType)
            : this()
        {
            Type = requestType;
        }

        public CustomerAccountRequestType Type { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
        
    }
}
