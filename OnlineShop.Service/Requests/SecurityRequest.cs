﻿using System.Collections.Generic;
using OnlineShop.Dto;

namespace OnlineShop.Service
{
    public class SecurityRequest : Request
    {
        public SecurityRequest()
        {
            RoleNamesToAdd = new List<string>();
        }

        public SecurityRequest(SecurityRequestType requestType)
            : this()
        {
            Type = requestType;
        }

        public SecurityRequestType Type { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public UserDto User { get; set; }

        public long UserId { get; set; }

        public bool AuthenticatedUserOnCreation { get; set; }

        public List<string> RoleNamesToAdd { get; set; }

        public List<int> RoleIdsToAdd { get; set; }
    }
}
