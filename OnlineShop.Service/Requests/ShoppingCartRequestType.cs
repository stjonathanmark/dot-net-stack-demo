﻿
namespace OnlineShop.Service
{
    public enum ShoppingCartRequestType
    {
        AddProduct,
        RemoveProduct,
        UpdateQuantity
    }
}
