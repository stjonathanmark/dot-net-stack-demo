﻿using System.Collections.Generic;
using OnlineShop.Dto;
using OnlineShop.Model;

namespace OnlineShop.Service
{
    public class CheckOutRequest : Request
    {
        public CheckOutRequest()
        {

        }

        public CheckOutRequest(CheckOutRequestType requestType)
        {
            Type = requestType;
        }

        public CheckOutRequestType Type { get; set; }

        public long CustomerProfileId { get; set; }

        public long? PaymentMethodId { get; set; }

        public CardDto Card { get; set; }

        public IEnumerable<ProductDto> Products { get; set; }

        public AddressDto ShippingAddress { get; set; }

        public decimal SubTotal { get; set; }

        public decimal Tax { get; set; }

        public decimal ShippingCost { get; set; }

        public decimal Total { get; set; }

        public bool SaveCard { get; set; }

        public string PaymentTokenId { get; set; }
        public string CreationTokenId { get; set; }
    }
}
