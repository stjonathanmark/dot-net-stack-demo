﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using OnlineShop.Data;
using OnlineShop.Model;
using OnlineShop.Model.Payments;
using OnlineShop.Model.Services;
using OnlineShop.Dto;

namespace OnlineShop.Service
{
    public class OnlineShopService : BaseService, IOnlineShopService
    {
        public IGeneralService generalService;
        public ICatalogService catalogService;
        public ICheckOutService checkOutService;
        public ICustomerAccountService customerAccountService;
        public ISecurityService securityService;

        public OnlineShopService()
        {
            DataSource context = new DataSource();

            IGeneralUnitOfWork generalUow = new GeneralUnitOfWork(context);
            generalService = new GeneralService(generalUow);

            ICatalogUnitOfWork catalogUow = new CatalogUnitOfWork(context);
            catalogService = new CatalogService(catalogUow);

            ICheckOutUnitOfWork checkOutUow = new CheckOutUnitOfWork(context);
            checkOutService = new CheckOutService(checkOutUow);

            ICustomerAccountUnitOfWork customerAccountUow = new CustomerAccountUnitOfWork(context);
            customerAccountService = new CustomerAccountService(customerAccountUow);

            ISecurityUnitOfWork securityUow = new SecurityUnitOfWork(context);
            securityService = new SecurityService(securityUow);
        }

        public CatalogResponse MakeRequest(CatalogRequest request)
        {
            CatalogResponse response = new CatalogResponse();

            try
            {
                switch(request.Type)
                {
                    case CatalogRequestType.GetAllCategories:
                        response = GetAllCategories();
                        break;
                    case CatalogRequestType.GetAllProducts:
                        response = GetAllProducts(request);
                        break;
                    case CatalogRequestType.GetCategoryByProduct:
                        response = GetCategoryByProduct(request);
                        break;
                    case CatalogRequestType.GetProductDetails:
                        response = GetProduct(request);
                        break;
                    case CatalogRequestType.GetProductsByCategory:
                        response = GetProductsByCategory(request);
                        break;
                    case CatalogRequestType.GetSubCategories:
                        response = GetSubCategories(request);
                        break;
                    case CatalogRequestType.SearchProducts:
                        response = SearchProducts(request);
                        break;
                    case CatalogRequestType.GetCategory:
                        response = GetCategory(request);
                        break;
                }

                response.Successful = true;
                if (string.IsNullOrEmpty(response.Message)) {
                    response.Message = Default.CatalogSuccessMessages[request.Type];
                }
            }
            catch (Exception ex)
            {
                GetExceptionInfo(ex, response, Default.CatalogErrorMessages[request.Type]);
            }

            return response;
        }

        public CheckOutResponse MakeRequest(CheckOutRequest request)
        {
            CheckOutResponse response = new CheckOutResponse();

            try
            {
                switch(request.Type)
                {
                    case CheckOutRequestType.GetPaymentOptions:
                        break;
                    case CheckOutRequestType.GetPaymentToken:
                        response = GetPaymentToken(request);
                        break;
                    case CheckOutRequestType.GetShippingOptions:
                        response = GetShippingOptions(request);
                        break;
                    case CheckOutRequestType.MakePayment:
                        response = MakePayment(request);
                        break;
                }

                response.Successful = true;
                if (string.IsNullOrEmpty(response.Message)) {
                    response.Message = Default.CheckOutSuccessMessages[request.Type];
                }
            }
            catch (Exception ex)
            {
                GetExceptionInfo(ex, response, Default.CheckOutErrorMessages[request.Type]);
            }

            return response;
        }

        public CustomerAccountResponse MakeRequest(CustomerAccountRequest request)
        {
            CustomerAccountResponse response = new CustomerAccountResponse();

            try
            {
                switch(request.Type)
                {
                    case CustomerAccountRequestType.ActivateAccount:
                        break;
                    case CustomerAccountRequestType.ChangePassword:
                        break;
                    case CustomerAccountRequestType.CreateAccount:
                        break;
                    case CustomerAccountRequestType.DeactivateAcount:
                        break;
                    case CustomerAccountRequestType.DisableAccount:
                        break;
                    case CustomerAccountRequestType.EnableAccount:
                        break;
                    case CustomerAccountRequestType.GetOrderDetails:
                        break;
                    case CustomerAccountRequestType.GetPastOrders:
                        break;
                    case CustomerAccountRequestType.GetPaymentMethodDetails:
                        break;
                    case CustomerAccountRequestType.GetPaymentMethods:
                        break;
                    case CustomerAccountRequestType.GetPersonalInfo:
                        break;
                    case CustomerAccountRequestType.GetProfileOverview:
                        break;
                    case CustomerAccountRequestType.Login:
                        break;
                    case CustomerAccountRequestType.Logout:
                        break;
                    case CustomerAccountRequestType.RemovePaymentMethod:
                        break;
                    case CustomerAccountRequestType.SearchPastOrder:
                        break;
                    case CustomerAccountRequestType.UpdatePaymentMethod:
                        break;
                }

                response.Successful = true;
                if (string.IsNullOrEmpty(response.Message))  {
                    response.Message = Default.CustomerAccountSuccessMessages[request.Type];
                }
            }
            catch (Exception ex)
            {
                response.Successful = false;
                GetExceptionInfo(ex, response, Default.CustomerAccountErrorMessages[request.Type]);
            }

            return response;
        }

        public SecurityResponse MakeRequest(SecurityRequest request)
        {
            SecurityResponse response = new SecurityResponse();
            
            try
            {
                switch(request.Type)
                {
                    case SecurityRequestType.AuthenticateUser:
                        response = AuthenticateUser(request);
                        break;
                    case SecurityRequestType.CreateUserAccount:
                        response = CreateUserAccount(request);
                        break;
                    case SecurityRequestType.GetUserForTokenCreation:
                        break;
                }

                response.Successful = true;
                if (string.IsNullOrEmpty(response.Message)) {
                    response.Message = Default.SecuritySuccessMessages[request.Type];
                }
            }
            catch (Exception ex)
            {
                GetExceptionInfo(ex, response, Default.SecurityErrorMessages[request.Type]);
            }

            return response;
        }

        public ShoppingCartResponse MakeRequest(ShoppingCartRequest request)
        {
            throw new System.NotImplementedException();
        }

        #region Security Functionality

        public SecurityResponse CreateUserAccount(SecurityRequest request)
        {
            SecurityResponse response = new SecurityResponse();

            User user = Mapper.Map<User>(request.User);
            
            response.AccountCreationResult = securityService.CreateAccount(user, request.RoleIdsToAdd, request.RoleNamesToAdd);
            response.AccountCreationMessage = Default.AccountCreationResultMessages[response.AccountCreationResult];

            if (response.AccountCreated = response.AccountCreationResult == AccountCreationResult.Successful)
            {
                if (request.AuthenticatedUserOnCreation)
                {
                    response.Authenticated = true;
                    user = securityService.GetUserForTokenCreation(user.Id);
                    response.TokenUser = Mapper.Map<TokenUserDto>(user);
                }
            }
            else
            {
                response.Message = "The request to create a user account was performed successfully, but due to business rules the user account was not able to be created with the provided information.";
            }

            return response;
        }

        public SecurityResponse AuthenticateUser(SecurityRequest request)
        {
            SecurityResponse response = new SecurityResponse();

            response.AuthenticationResult = securityService.AuthenticateUser(request.Username, request.Password);
            response.AuthenticationMessage = Default.AuthenticationResultMessages[response.AuthenticationResult];

            if (response.AuthenticationResult == AuthenticationResult.Successful)
            {
                response.Authenticated = true;

                long userId = securityService.GetUserIdByUsername(request.Username);

                User user = securityService.GetUserForTokenCreation(userId);

                response.TokenUser = Mapper.Map<TokenUserDto>(user);
            }

            return response;
        }

        #endregion

        #region Check Out Functionality

        private CheckOutResponse GetShippingOptions(CheckOutRequest request)
        {
            CheckOutResponse response = new CheckOutResponse();

            response.States = Mapper.Map<IEnumerable<StateDto>>(generalService.GetAllStates());

            return response;
        }

        private CheckOutResponse GetPaymentToken(CheckOutRequest request)
        {
            CheckOutResponse response = new CheckOutResponse();

            Card card = Mapper.Map<Card>(request.Card);

            PaymentToken paymentToken = checkOutService.GetPaymentToken(card);

            response.PaymentToken = Mapper.Map<PaymentTokenDto>(paymentToken);

            return response;
        }

        private CheckOutResponse MakePayment(CheckOutRequest request)
        {
            CheckOutResponse response =  new CheckOutResponse();

            IEnumerable<OrderedItemDto> orderedItems = GetOrderedItems(request.Products);

            OrderDto orderDto = new OrderDto();
            orderDto.CustomerProfileId = request.CustomerProfileId;
            orderDto.OrderedItems = orderedItems.ToList();
            orderDto.ShippingAddress = request.ShippingAddress;
            orderDto.SubTotal = request.SubTotal;
            orderDto.Tax = request.Tax;
            orderDto.ShippingCost = request.ShippingCost;
            orderDto.Total = request.Total;


            Order order = Mapper.Map<Order>(orderDto);
            Payment payment = null;

            if (request.PaymentMethodId.HasValue)
            {
                payment = checkOutService.MakePayment(request.PaymentMethodId.Value, order, true);
            }
            else
            {
                payment = checkOutService.MakePayment(request.PaymentTokenId, request.CreationTokenId, order, true);
            }

            response.PaymentApproved = !payment.Failed;

            if (response.PaymentApproved)
            {
                response.PaymentMessage = "Your payment was approved.";
                response.Order = Mapper.Map<OrderDto>(order);
            }
            else
            {
                response.Message = "The request to make the payment was performed successfully, however due to other reasons the payment failed.";
                response.PaymentMessage = payment.FailureMessage;

            }

            return response;
        }
        
        private IEnumerable<OrderedItemDto> GetOrderedItems(IEnumerable<ProductDto> products)
        {
            List<OrderedItemDto> orderedItems = new List<OrderedItemDto>();

            foreach (ProductDto product in products)
            {
                OrderedItemDto item = new OrderedItemDto()
                {
                    ProductId = product.Id,
                    Price = product.Price,
                    Quantity = product.Quantity
                };

                if (product.ChosenOptions.Any())
                {
                    foreach (int optionId in product.ChosenOptions)
                    {
                        item.OrderedItemOptions.Add(new OrderedItemOptionDto() { OptionId = optionId });
                    }
                }

                orderedItems.Add(item);
            }

            return orderedItems;

        }

        #endregion

        #region Customer Account Functionality

        #endregion

        #region Catalog Functionality

        private CatalogResponse GetAllCategories()
        {
            CatalogResponse response = new CatalogResponse
            {
                Categories = Mapper.Map<IEnumerable<CategoryDto>>(catalogService.GetAllCategories())
            };

            return response;
        }

        private CatalogResponse GetAllProducts(CatalogRequest request)
        {
            CatalogResponse response = new CatalogResponse();

            IEnumerable<Product> products = catalogService.GetAllProducts(request.Skip, request.Take);
            response.Products = Mapper.Map<IEnumerable<ProductDto>>(products);
            response.PagingInfo = GetPagingInfo(request, (int)catalogService.RepositoryInfo["ProductsFound"]);

            return response;
        }

        private CatalogResponse GetCategoryByProduct(CatalogRequest request)
        {
            CatalogResponse response = new CatalogResponse
            {
                Category = Mapper.Map<CategoryDto>(catalogService.GetCategoryByProduct(request.ProductId.Value))
            };

            return response;
        }
        
        private CatalogResponse GetProduct(CatalogRequest request)
        {
            CatalogResponse response = new CatalogResponse
            {
                Product = Mapper.Map<ProductDto>(catalogService.GetProduct(request.ProductId.Value))
            };

            return response;
        }

        private CatalogResponse GetProductsByCategory(CatalogRequest request)
        {
            CatalogResponse response = new CatalogResponse
            {
                Products = Mapper.Map<IEnumerable<ProductDto>>(catalogService.GetProductsByCategory(request.CategoryId.Value, request.Skip, request.Take)),
                PagingInfo = GetPagingInfo(request, (int)catalogService.RepositoryInfo["ProductsFound"])
            };

            return response;
        }

        private CatalogResponse SearchProducts(CatalogRequest request)
        {
            CatalogResponse response = new CatalogResponse
            {
                Products = Mapper.Map<IEnumerable<ProductDto>>(catalogService.SearchProducts(p => p.Name.ToLower().Contains(request.SearchTerm.ToLower()), request.Skip, request.Take)),
                PagingInfo = GetPagingInfo(request, (int)catalogService.RepositoryInfo["ProductsFound"])
            };

            return response;
        }

        private CatalogResponse GetSubCategories(CatalogRequest request)
        {
            CatalogResponse response = new CatalogResponse
            {
                Categories = Mapper.Map<IEnumerable<CategoryDto>>(catalogService.GetSubCategories(request.CategoryId.Value))
            };

            return response;
        }

        private CatalogResponse GetCategory(CatalogRequest request)
        {
            CatalogResponse response = new CatalogResponse()
            {
                Category = Mapper.Map<CategoryDto>(catalogService.GetCategory(request.CategoryId.Value))
            };

            return response;
        }

        #endregion
    }
}
