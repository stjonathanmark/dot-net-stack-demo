﻿namespace OnlineShop.Service
{
    public interface IOnlineShopService
    {
        CatalogResponse MakeRequest(CatalogRequest request);
        CheckOutResponse MakeRequest(CheckOutRequest request);
        CustomerAccountResponse MakeRequest(CustomerAccountRequest request);
        ShoppingCartResponse MakeRequest(ShoppingCartRequest request);
    }
}