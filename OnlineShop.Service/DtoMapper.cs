﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using OnlineShop.Data;
using OnlineShop.Model;
using OnlineShop.Model.Payments;
using OnlineShop.Dto;
using AutoMapper;

namespace OnlineShop.Service
{
    public class DtoMapper
    {
        public static void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {


                cfg.CreateMap<Product, ProductDto>().PreserveReferences();
                cfg.CreateMap<ProductDto, Product>().PreserveReferences();

                cfg.CreateMap<Order, OrderDto>().PreserveReferences();
                cfg.CreateMap<OrderDto, Order>().PreserveReferences();

                cfg.CreateMap<Address, AddressDto>().PreserveReferences();
                cfg.CreateMap<AddressDto, Address>().PreserveReferences();

                cfg.CreateMap<BillingAddress, BillingAddressDto>().PreserveReferences();
                cfg.CreateMap<BillingAddressDto, BillingAddress>().PreserveReferences();

                cfg.CreateMap<BillingInformation, BillingInformationDto>().PreserveReferences();
                cfg.CreateMap<BillingInformationDto, BillingInformation>().PreserveReferences();

                cfg.CreateMap<Card, CardDto>().PreserveReferences();
                cfg.CreateMap<CardDto, Card>().PreserveReferences();

                cfg.CreateMap<Category, CategoryDto>().PreserveReferences();
                cfg.CreateMap<CategoryDto, Category>().PreserveReferences();

                cfg.CreateMap<Country, CountryDto>().PreserveReferences();
                cfg.CreateMap<CountryDto, Country>().PreserveReferences();

                cfg.CreateMap<CustomerProfile, CustomerProfileDto>().PreserveReferences();
                cfg.CreateMap<CustomerProfileDto, CustomerProfile>().PreserveReferences();

                cfg.CreateMap<EmailAddress, EmailAddressDto>().PreserveReferences();
                cfg.CreateMap<EmailAddressDto, EmailAddress>().PreserveReferences();

                cfg.CreateMap<Option, OptionDto>().PreserveReferences();
                cfg.CreateMap<OptionDto, Option>().PreserveReferences();

                cfg.CreateMap<OptionSet, OptionSetDto>().PreserveReferences();
                cfg.CreateMap<OptionSetDto, OptionSet>().PreserveReferences();

                cfg.CreateMap<OrderedItem, OrderedItemDto>().PreserveReferences();
                cfg.CreateMap<OrderedItemDto, OrderedItem>().PreserveReferences();

                cfg.CreateMap<OrderedItemOption, OrderedItemOptionDto>().PreserveReferences();
                cfg.CreateMap<OrderedItemOptionDto, OrderedItemOption>().PreserveReferences();

                cfg.CreateMap<Password, PasswordDto>().PreserveReferences();
                cfg.CreateMap<PasswordDto, Password>().PreserveReferences();

                cfg.CreateMap<Payment, PaymentDto>().PreserveReferences();
                cfg.CreateMap<PaymentDto, Payment>().PreserveReferences();

                cfg.CreateMap<PaymentMethod, PaymentMethodDto>().PreserveReferences();
                cfg.CreateMap<PaymentMethodDto, PaymentMethod>().PreserveReferences();

                cfg.CreateMap<PhoneNumber, PhoneNumberDto>().PreserveReferences();
                cfg.CreateMap<PhoneNumberDto, PhoneNumber>().PreserveReferences();

                cfg.CreateMap<Role, RoleDto>().PreserveReferences();
                cfg.CreateMap<RoleDto, Role>().PreserveReferences();

                cfg.CreateMap<RoleSubRole, RoleSubRoleDto>().PreserveReferences();
                cfg.CreateMap<RoleSubRoleDto, RoleSubRole>().PreserveReferences();

                cfg.CreateMap<State, StateDto>().PreserveReferences();  
                cfg.CreateMap<StateDto, State>().PreserveReferences();

                cfg.CreateMap<User, UserDto>().PreserveReferences();
                cfg.CreateMap<UserDto, User>().PreserveReferences();

                cfg.CreateMap<UserRole, UserRoleDto>().PreserveReferences();
                cfg.CreateMap<UserRoleDto, UserRole>().PreserveReferences();

                cfg.CreateMap<PaymentToken, PaymentTokenDto>().PreserveReferences();
                cfg.CreateMap<PaymentTokenDto, PaymentToken>().PreserveReferences();
                
                cfg.CreateMap<User, TokenUserDto>().ConvertUsing<TokenUserConverter>();
            });

            Mapper.Configuration.CompileMappings();
        }

        class TokenUserConverter : ITypeConverter<User, TokenUserDto>
        {
            List<int> roleIds = new List<int>();
            List<TokenRoleDto> tokenRoles = new List<TokenRoleDto>();

            public TokenUserDto Convert(User source, TokenUserDto destination, ResolutionContext context)
            {
                GetAllDistinctRoles(source.UserRoles.Select(ur => ur.Role));

                TokenUserDto tokenUser = new TokenUserDto()
                {
                    UserId = source.Id,
                    Username = source.Username,
                    Roles = tokenRoles
                };

                if (source.CustomerProfile != null)
                {
                    tokenUser.CustomerId = source.CustomerProfile.Id;
                    tokenUser.CompanyName = (!source.CustomerProfile.IsPersonal) ? source.CustomerProfile.CompanyName : "";
                    tokenUser.FullName = source.CustomerProfile.FirstName + " " + source.CustomerProfile.LastName;
                }

                return tokenUser;
            }

            private void GetAllDistinctRoles(IEnumerable<Role> roles)
            {
                if (roles != null && roles.Any())
                {
                    foreach (Role role in roles)
                    {
                        if (!roleIds.Contains(role.Id))
                        {
                            roleIds.Add(role.Id);
                            tokenRoles.Add(new TokenRoleDto() { Id = role.Id, Name = role.Name });
                            if (role.RoleSubRoles != null && role.RoleSubRoles.Any())
                            {
                                GetAllDistinctRoles(role.RoleSubRoles.Select(rsr => rsr.SubRole));
                            }
                        }
                    }
                }
            }
        }
    }
}
