﻿using System.Collections.Generic;

namespace OnlineShop.Service
{
    public static class Default
    {
        private const string baseErrorMsg = "An error occurred ";
        private const string baseSuccessMsg = "";

        #region Response Messages

        public static Dictionary<CatalogRequestType, string> CatalogErrorMessages = new Dictionary<CatalogRequestType, string>()
        {
            { CatalogRequestType.GetAllCategories, baseErrorMsg + "getting all categories." },
            { CatalogRequestType.GetAllProducts, baseErrorMsg + "getting all products." },
            { CatalogRequestType.GetCategoryByProduct, baseErrorMsg + "getting all category by product." },
            { CatalogRequestType.GetProductDetails, baseErrorMsg + "getting product details." },
            { CatalogRequestType.GetProductsByCategory, baseErrorMsg + "getting products by category." },
            { CatalogRequestType.GetSubCategories, baseErrorMsg + "getting sub categories." },
            { CatalogRequestType.SearchProducts, baseErrorMsg + "searching products." },
            { CatalogRequestType.GetCategory, baseErrorMsg + "getting category." }
        };

        public static Dictionary<CatalogRequestType, string> CatalogSuccessMessages = new Dictionary<CatalogRequestType, string>()
        {
            { CatalogRequestType.GetAllCategories, "All categories were retrieved successfully." },
            { CatalogRequestType.GetAllProducts, "All products were retrieved successfully." },
            { CatalogRequestType.GetCategoryByProduct, "Category was retrieved by product successfuly." },
            { CatalogRequestType.GetProductDetails, "Product details were retrieved successfully." },
            { CatalogRequestType.GetProductsByCategory, "Products were retrieved by category successfully." },
            { CatalogRequestType.GetSubCategories, "Sub categories were retrieved successfully." },
            { CatalogRequestType.SearchProducts, "Matching products were retrieved successfully." },
            { CatalogRequestType.GetCategory, "Category was retrieved successfully." }
        };



        public static Dictionary<CheckOutRequestType, string> CheckOutErrorMessages = new Dictionary<CheckOutRequestType, string>()
        {
            { CheckOutRequestType.GetPaymentOptions, baseErrorMsg + "getting payment methods." },
            { CheckOutRequestType.GetPaymentToken, baseErrorMsg + "getting payment token." },
            { CheckOutRequestType.GetShippingOptions, baseErrorMsg + "getting shipping options." },
            { CheckOutRequestType.MakePayment, baseErrorMsg + "making payment." }
        };

        public static Dictionary<CheckOutRequestType, string> CheckOutSuccessMessages = new Dictionary<CheckOutRequestType, string>()
        {
            { CheckOutRequestType.GetPaymentOptions, "Payment methods were retrieved successfully." },
            { CheckOutRequestType.GetPaymentToken, "Payment token was retrieved successfully." },
            { CheckOutRequestType.GetShippingOptions, "Shipping options were retrieved successfully." },
            { CheckOutRequestType.MakePayment, "Payment was made successfully." }
        };


        public static Dictionary<CustomerAccountRequestType, string> CustomerAccountErrorMessages = new Dictionary<CustomerAccountRequestType, string>()
        {
            { CustomerAccountRequestType.Login, baseErrorMsg + "while authentication the user." }
        };

        public static Dictionary<CustomerAccountRequestType, string> CustomerAccountSuccessMessages = new Dictionary<CustomerAccountRequestType, string>()
        {
            { CustomerAccountRequestType.Login,  "The request to authenticate the user was performed successfully."}
        };


        public static Dictionary<SecurityRequestType, string> SecurityErrorMessages = new Dictionary<SecurityRequestType, string>()
        {
            { SecurityRequestType.AuthenticateUser, baseErrorMsg + "authenticating user." },
            { SecurityRequestType.CreateUserAccount, baseErrorMsg + "creating user account." },
            { SecurityRequestType.GetUserForTokenCreation, baseErrorMsg + "geting user for token creation." }
        };

        public static Dictionary<SecurityRequestType, string> SecuritySuccessMessages = new Dictionary<SecurityRequestType, string>()
        {
            { SecurityRequestType.AuthenticateUser, "The request to authenticate the user was performed successfully." },
            { SecurityRequestType.CreateUserAccount, "User account was created successfully." },
            { SecurityRequestType.GetUserForTokenCreation, "User was retrieved for token creation successfully." }
        };

        #endregion

        public static Dictionary<AuthenticationResult, string> AuthenticationResultMessages = new Dictionary<AuthenticationResult, string>()
        {
            { AuthenticationResult.Successful, "User was authenticated successfully." },
            { AuthenticationResult.InvalidPassword, "Invalid username and/or password." },
            { AuthenticationResult.UserDoesNotExist, "Invalid username and/or password." },
            { AuthenticationResult.Inactive, "Your account has not been activated." },
            { AuthenticationResult.Disabled, "Your account as been disabled." },
            { AuthenticationResult.LockedOut, "Your account has been locked." },
            { AuthenticationResult.PasswordExpired, "Your password has expired." }
        };

        public static Dictionary<AccountCreationResult, string> AccountCreationResultMessages = new Dictionary<AccountCreationResult, string>()
        {
            { AccountCreationResult.Successful, "User account was created successfully." },
            { AccountCreationResult.UsernameAlreadyExists, "The username provided is already in use." }
        };
    }
}
