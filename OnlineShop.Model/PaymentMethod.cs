﻿using System;
using System.Collections.Generic;

namespace OnlineShop.Model
{
    public class PaymentMethod : BasePaymentMethod
    {
        public PaymentMethod()
        {
            Orders = new List<Order>();
        }

        public CustomerProfile CustomerProfile { get; set; }

        public List<Order> Orders { get; set; }

        public string FormattedExpirationDate
        {
            get
            {
                return this.ExpirationDate.ToString("MM/yyyy");
            }
        }

        public bool IsExpired
        {
            get
            {
                return ExpirationDate < DateTime.Now;
            }
        }
    }
}
