﻿
using System.Collections.Generic;

namespace OnlineShop.Model
{
    public class Product : BaseProduct
    {
        public Product()
        {
            OptionSets = new List<OptionSet>();
            OrderedItems = new List<OrderedItem>();
        }

        public Category Category { get; set; }

        public List<OptionSet> OptionSets { get; set; }

        public List<OrderedItem> OrderedItems { get; set; }
    }
}
