﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShop.Model.Payments
{
    public class Card : BaseCard
    {
        public Card()
        {
            Address = new BillingAddress();
        }

        public BillingAddress Address { get; set; }
    }
}
