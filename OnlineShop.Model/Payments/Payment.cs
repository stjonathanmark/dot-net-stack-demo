﻿
using System;

namespace OnlineShop.Model.Payments
{
    public class Payment : BasePayment
    {
        internal Card Card { get; set; }

        internal string SourceCardId { get; set; }

        public override bool Succeeded {
            get { return Status == "succeeded"; }
            set { Status = value ? "succeeded" : Status; }
        }

        public override bool Pending {
            get { return Status == "pending"; }
            set { Status = value ? "pending" : Status; }
        }

        public override bool Failed {
            get { return Status == "failed"; }
            set { Status = value ? "failed" : Status; }
        }

        public override decimal Amount
        {
            get
            {
                return ((decimal)AmountInCents) / 100;
            }

            set
            {
                AmountInCents = (int)(value * 100);
            }
        }
    }
}
