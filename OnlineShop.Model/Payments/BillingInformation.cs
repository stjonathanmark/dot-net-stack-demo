﻿using System.Linq;

namespace OnlineShop.Model.Payments
{
    public class BillingInformation : BaseBillingInformation
    {

        public BillingInformation()
        {
            Card = new Card();
        }

        public BillingInformation(CustomerProfile customerProfile)
        {
            FirstName = customerProfile.FirstName;
            LastName = customerProfile.LastName;
            EmailAddress = customerProfile.EmailAddresses.First(em => em.IsPrimary).Address;
        }

        public BillingInformation(CustomerProfile customerProfile, Card card)
            : this(customerProfile)
        {
            Card = card;
        }

        public BillingInformation(CustomerProfile customerProfile, string tokenId)
            : this(customerProfile)
        {
            TokenId = tokenId;
        }

        public string TokenId { get; set; }

        public Card Card { get; set; }
    }
}
