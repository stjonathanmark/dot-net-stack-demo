﻿
namespace OnlineShop.Model.Payments
{
    public class PaymentToken : BasePaymentToken
    {
        public PaymentMethod PaymentMethod { get; set; }
    }
}
