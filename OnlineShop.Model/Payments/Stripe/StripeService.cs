﻿using System;
using Stripe;
using System.Collections.Generic;
using System.Linq;

namespace OnlineShop.Model.Payments.Stripe
{
    public class StripeService
    {
        private Dictionary<int, int> lastDayOfMonth = new Dictionary<int, int>() {
            { 1, 31 },
            { 2, 28 },
            { 3, 31 },
            { 4, 30 },
            { 5, 31 },
            { 6, 30 },
            { 7, 31 },
            { 8, 31 },
            { 9, 30 },
            { 10, 31 },
            { 11, 30 },
            { 12, 31 }
        };

        public StripeService()
        {
            StripeConfiguration.SetApiKey("Enter_Your_Secret_API_Key_From__Your_Stripe_Account");
        }

        public PaymentMethod CreateCustomer(BillingInformation billingInfo)
        {
            var customerOptions = new StripeCustomerCreateOptions()
            {
                Metadata = GetMetaData(billingInfo),
                Description = GetDescription(billingInfo),
                Email = billingInfo.EmailAddress,
                SourceToken = billingInfo.TokenId
            };

            var customerService = new StripeCustomerService();
            var customer = customerService.Create(customerOptions);

            PaymentMethod method = GetPaymentMethod(customer.Sources.First().Card);

            return method;
        }
        
        public PaymentMethod CreateSourceCard(string customerId, string tokenId)
        {
            var cardOptions = new StripeCardCreateOptions()
            {
                SourceToken = tokenId,
            };

            var cardService = new StripeCardService();
            var sourceCard = cardService.Create(customerId, cardOptions);

            PaymentMethod method = GetPaymentMethod(sourceCard);

            return method;
        }

        public PaymentToken CreateSourceToken(Card card)
        {
            var sourceCard = GetSourceCard(card);

            var tokenOptions = new StripeTokenCreateOptions()
            {
                Card = new StripeCreditCardOptions() {
                    Name = sourceCard.Name,
                    Number = sourceCard.Number,
                    ExpirationMonth = sourceCard.ExpirationMonth,
                    ExpirationYear = sourceCard.ExpirationYear,
                    Cvc = sourceCard.Cvc,

                    AddressLine1 = sourceCard.AddressLine1,
                    AddressLine2 = sourceCard.AddressLine2,
                    AddressCity = sourceCard.AddressCity,
                    AddressState = sourceCard.AddressState,
                    AddressZip = sourceCard.AddressZip,
                    AddressCountry = sourceCard.AddressCountry
                }

            };

            var tokenService = new StripeTokenService();

            var payToken = tokenService.Create(tokenOptions);
            var createToken = tokenService.Create(tokenOptions);

            var paymentToken = new PaymentToken()
            {
                TokenId = payToken.Id,
                CreateTokenId = createToken.Id,
                PaymentMethod = GetPaymentMethod(payToken.StripeCard)
            };

            return paymentToken;
        }

        public Payment CreateCharge(decimal amount, string customerId, string sourceId, bool capture = false)
        {
            var chargeOptions = new StripeChargeCreateOptions()
            {
                Amount = (int)(amount * 100),
                Capture = capture,
                Currency = "USD",
                CustomerId = customerId,
                SourceTokenOrExistingSourceId = sourceId
            };

            var chargeService = new StripeChargeService();
            var charge = chargeService.Create(chargeOptions);

            return new Payment()
            {
                Id = charge.Id,
                PaymentDate = charge.Created,
                Status = charge.Status,
                FailureCode = charge.FailureCode,
                FailureMessage = charge.FailureMessage,
                AmountInCents = charge.Amount,
                Paid = charge.Paid,
                Captured = charge.Captured ?? true
            };
        }

        public Payment CreateCharge(decimal amount, string tokenId, bool capture = false)
        {
            var chargeOptions = new StripeChargeCreateOptions()
            {
                Amount = (int)(amount * 100),
                Capture = capture,
                Currency = "USD",
                SourceTokenOrExistingSourceId = tokenId
            };

            var chargeService = new StripeChargeService();
            var charge = chargeService.Create(chargeOptions);
            var sourceCardId = charge.Source.Id;
            return new Payment()
            {
                Id = charge.Id,
                PaymentDate = charge.Created,
                Status = charge.Status,
                FailureCode = charge.FailureCode,
                FailureMessage = charge.FailureMessage,
                AmountInCents = charge.Amount,
                Paid = charge.Paid,
                Captured = charge.Captured ?? true
            };
        }

        private string GetDescription(BillingInformation billingInfo)
        {
            string description = $"{billingInfo.FirstName} {billingInfo.LastName}";

            return description;
        }

        private Dictionary<string, string> GetMetaData(BillingInformation billingInfo)
        {
            var metaData = new Dictionary<string, string>();

            return metaData;
        }

        private SourceCard GetSourceCard(Card card)
        {
            var sourceCard = new SourceCard()
            {
                Name = card.Name,
                ExpirationMonth = card.ExpirationMonth.Value,
                ExpirationYear = card.ExpirationYear.Value,
                Number = card.Number,
                Cvc = card.Cvc
            };

            if (card.Address != null)
            {
                sourceCard.AddressLine1 = card.Address.StreetOne;
                sourceCard.AddressLine2 = card.Address.StreetTwo;
                sourceCard.AddressCity = card.Address.City;
                sourceCard.AddressState = card.Address.State;
                sourceCard.AddressZip = card.Address.ZipCode;
                sourceCard.AddressCountry = card.Address.Country;
            }

            return sourceCard;
        }

        private Card GetCard(SourceCard sourceCard)
        {
            Card card = new Card()
            {
                Name = sourceCard.Name,
                Number = sourceCard.Number,
                Cvc = sourceCard.Cvc,
                ExpirationMonth = sourceCard.ExpirationMonth,
                ExpirationYear = sourceCard.ExpirationYear,
                Address = new BillingAddress()
                {
                    StreetOne = sourceCard.AddressLine1,
                    StreetTwo = sourceCard.AddressLine2,
                    City = sourceCard.AddressCity,
                    State = sourceCard.AddressState,
                    ZipCode = sourceCard.AddressZip,
                    Country = sourceCard.AddressCountry
                }
            };

            return card;
        }

        private PaymentMethod GetPaymentMethod(StripeCard card)
        {
            bool expYearIsLeapYear = card.ExpirationYear % 4 == 0;
            bool expMonthIsFeb = card.ExpirationMonth == 2;

            int expirationDay = (expYearIsLeapYear && expMonthIsFeb) ? 29 : lastDayOfMonth[card.ExpirationMonth];

            return new PaymentMethod()
            {
                CustomerId = card.CustomerId,
                CardType = card.Brand,
                SourceCardId = card.Id,
                ExpirationDate = new DateTime(card.ExpirationYear, card.ExpirationMonth, expirationDay),
                LastFourDigits = card.Last4
            };
        }
    }
}
