﻿
namespace OnlineShop.Model
{
    public class UserRole : BaseUserRole
    {
        public User User { get; set; }

        public Role Role { get; set; }
    }
}
