﻿using System.Collections.Generic;

namespace OnlineShop.Model
{
    public class User : BaseUser
    {
        public User()
        {
            UserRoles = new List<UserRole>();
            EmailAddresses = new List<EmailAddress>();
            Passwords = new List<Password>();
        }

        public CustomerProfile CustomerProfile { get; set; }

        public List<UserRole> UserRoles { get; set; }

        public List<EmailAddress> EmailAddresses { get; set; }

        public List<Password> Passwords { get; set; }
    }
}
