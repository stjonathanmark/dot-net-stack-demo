﻿using System.Collections.Generic;

namespace OnlineShop.Model
{
    public class OrderedItem : BaseOrderedItem
    {
        public OrderedItem()
        {
            OrderedItemOptions = new List<OrderedItemOption>();
        }

        public Order Order { get; set; }

        public Product Product { get; set; }

        public List<OrderedItemOption> OrderedItemOptions { get; set; }
        
    }
}
