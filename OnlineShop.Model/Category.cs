﻿
using System.Collections.Generic;

namespace OnlineShop.Model
{
    public class Category : BaseCategory
    {
        public Category()
        {
            SubCategories = new List<Category>();
            Products = new List<Product>();
        }

        public Category ParentCategory { get; set; }

        public List<Category> SubCategories { get; set; }

        public List<Product> Products { get; set; }
    }
}
