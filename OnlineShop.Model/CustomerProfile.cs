﻿using System.Collections.Generic;

namespace OnlineShop.Model
{
    public class CustomerProfile : BaseCustomerProfile
    {
        public CustomerProfile()
        {
            Addresses = new List<Address>();
            EmailAddresses = new List<EmailAddress>();
            PhoneNumbers = new List<PhoneNumber>();
            PaymentMethods = new List<PaymentMethod>();
        }

        public User User { get; set; }

        public List<Address> Addresses { get; set; }

        public List<EmailAddress> EmailAddresses { get; set; }

        public List<PhoneNumber> PhoneNumbers { get; set; }

        public List<PaymentMethod> PaymentMethods { get; set; }
    }
}
