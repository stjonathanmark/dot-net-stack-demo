﻿using System.Collections.Generic;

namespace OnlineShop.Model
{
    public class Option : BaseOption
    {
        public Option()
        {
            OrderedItemOptions = new List<OrderedItemOption>();
        }

        public OptionSet OptionSet { get; set; }

        public List<OrderedItemOption> OrderedItemOptions { get; set; }
    }
}
