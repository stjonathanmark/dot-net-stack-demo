﻿using System;
using System.Collections.Generic;
using System.Linq;
using OnlineShop.Data;


namespace OnlineShop.Model.Services
{
    public class SecurityService : ISecurityService
    {
        ISecurityUnitOfWork unitOfWork;

        public SecurityService(ISecurityUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public AuthenticationResult AuthenticateUser(string username, string password)
        {
            AuthenticationResult result = AuthenticationResult.Successful;

            User user = unitOfWork.Users.GetByUsername(username);

            if (user == null)
            {
                result = AuthenticationResult.UserDoesNotExist;
            }
            else
            {
                if (user.FailedLoginAttempts > 1000)
                {
                    user.LockedOut = true;
                    result = AuthenticationResult.LockedOut;
                }
                else
                {
                    //TODO: Hash value of password parameter
                    string hashedPassword = HashPassword(password);

                    if (user.Password == hashedPassword)
                    {
                        if (!user.Activated)
                        {
                            result = AuthenticationResult.Inactive;
                        }
                        else if (!user.Enabled)
                        {
                            result = AuthenticationResult.Disabled;
                        }
                        else
                        {
                            //TODO: Check for password expiration
                            //result = AuthenticationResult.PasswordExpired;
                        }

                        if (user.FailedLoginAttempts > 0)
                        {
                            user.FailedLoginAttempts = 0;
                            unitOfWork.Commit();
                        }
                    }
                    else
                    {
                        user.FailedLoginAttempts++;
                        if (user.FailedLoginAttempts > 1000)
                        {
                            user.FailedLoginAttempts = 0;
                        }
                        unitOfWork.Commit();
                        result = AuthenticationResult.InvalidPassword;
                    }
                }
            }

            return result;
        }

        public AccountCreationResult CreateAccount(User user, IEnumerable<int> roleIdsToAdd = null, IEnumerable<string> roleNamesToAdd = null)
        {
            AccountCreationResult result = AccountCreationResult.Successful;

            bool usernameExists = unitOfWork.Users.UsernameExists(user.Username);

            if (usernameExists)
            {
                result = AccountCreationResult.UsernameAlreadyExists;
            }
            else
            {
                AddRolesToUser(user, roleIdsToAdd, roleNamesToAdd);
                user.Password = HashPassword(user.Password);
                user.Enabled = user.Activated = true;
                user.LastPasswordChangeDate = user.CreationDate = DateTime.Now;
                user.Salt = "salt";

                unitOfWork.Users.Add(user);
                unitOfWork.Commit();
            }

            return result;
        }

        public User GetUserForTokenCreation(long userId)
        {
            User user = unitOfWork.Users.GetForTokenCreation(userId);

            return user;
        }

        public long GetUserIdByUsername(string username)
        {
            long userId = unitOfWork.Users.GetIdByUsername(username);

            return userId;
        }

        public Role GetRoleById(int roleId)
        {
            Role role = unitOfWork.Roles.Get(roleId);

            return role;
        }

        public IEnumerable<Role> GetRolesByIds(IEnumerable<int> roleIds)
        {
            IEnumerable<Role> roles = unitOfWork.Roles.Get(roleIds);

            return roles;
        }

        public Role GetRoleByName(string roleName)
        {
            Role role = unitOfWork.Roles.GetByName(roleName);

            return role;
        }

        public IEnumerable<Role> GetRolesByNames(IEnumerable<string> roleNames)
        {
            IEnumerable<Role> roles = unitOfWork.Roles.GetByNames(roleNames);

            return roles;
        }
 
        private string HashPassword(string password)
        {
            //TODO: Implement HashPassword Method
            string hashedPassword = password;

            return hashedPassword;
        }

        private void AddRolesToUser(User user, IEnumerable<int> roleIdsToAdd = null, IEnumerable<string> roleNamesToAdd = null)
        {
            List<Role> roles = new List<Role>();

            if (roleIdsToAdd != null && roleIdsToAdd.Any())
            {
                roles.AddRange(unitOfWork.Roles.Get(roleIdsToAdd));
            }

            if (roleNamesToAdd != null && roleNamesToAdd.Any())
            {
                roles.AddRange(unitOfWork.Roles.GetByNames(roleNamesToAdd));
            }

            if (roles.Any())
            {
                foreach (Role role in roles)
                {
                    user.UserRoles.Add(new UserRole() { RoleId = role.Id, Role = role });
                }
            }
        }
    }
}
