﻿using System.Collections.Generic;
using System;

namespace OnlineShop.Model.Services
{
    public interface ICatalogService
    {
        IEnumerable<Category> GetAllCategories();

        IEnumerable<Category> GetSubCategories(int categoryId);

        IEnumerable<Product> GetAllProducts(int? skip = null, int? take = null);

        IEnumerable<Product> GetProductsByCategory(int categoryId, int? skip = null, int? take = null);

        IEnumerable<Product> SearchProducts(Func<Product, bool> predicate, int? skip = null, int? take = null);

        Category GetCategory(int categoryId);

        Category GetCategoryByProduct(int productId);

        Product GetProduct(int productId);

        Dictionary<string, object> RepositoryInfo { get; }
    }
}
