﻿using System.Collections.Generic;
using System.Linq;
using OnlineShop.Data;
using OnlineShop.Model.Payments;
using OnlineShop.Model.Payments.Stripe;

namespace OnlineShop.Model.Services
{
    public class CheckOutService : ICheckOutService
    {
        ICheckOutUnitOfWork unitOfWork;

        public CheckOutService(ICheckOutUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public IEnumerable<OrderedItem> GetOrderedItems(IEnumerable<Product> products, Dictionary<int, int> quantities, Dictionary<int, IEnumerable<int>> Options = null)
        {
            List<OrderedItem> orderedItems = new List<OrderedItem>();

            foreach (Product product in products)
            {
                int id = product.Id;

                OrderedItem item = new OrderedItem()
                {
                    ProductId = id,
                    Quantity = quantities[id],
                    Price = product.Price
                };

                if (Options != null && Options.Any() && Options.ContainsKey(id))
                {
                    IEnumerable<int> optionIds = Options[id];

                    foreach (int optionId in optionIds)
                    {
                        item.OrderedItemOptions.Add(new OrderedItemOption()
                        {
                            OptionId = optionId
                        });
                    }
                }

                orderedItems.Add(item);
            }

            return orderedItems;
        }

        public Order CreateOrder(long customerProfileId, Address shippingAddress, IEnumerable<OrderedItem> orderedItems)
        {
            Order order = new Order();

            order.OrderedItems = new List<OrderedItem>(orderedItems);
            order.CustomerProfileId = customerProfileId;
            order.ShippingAddress = shippingAddress;
            order.Status = OrderStatus.Pending;

            unitOfWork.Orders.Add(order);
            unitOfWork.Commit();

            return order;
        }

        public PaymentMethod CreatePaymentMethod(long customerProfileId, string tokenId)
        {
            CustomerProfile profile = unitOfWork.CustomerProfiles.GetWithContactInformation(customerProfileId);

            StripeService stripe = new StripeService();

            PaymentMethod method = null;

            if (string.IsNullOrEmpty(profile.PaymentGatewayId))
            {
                BillingInformation billingInfo = new BillingInformation(profile, tokenId);
                method = stripe.CreateCustomer(billingInfo);
                profile.PaymentGatewayId = method.CustomerId;
            }
            else
            {
                method = stripe.CreateSourceCard(profile.PaymentGatewayId, tokenId);
            }
            
            profile.PaymentMethods.Add(method);

            unitOfWork.Commit();
            
            return method;
        }

        public PaymentToken GetPaymentToken(Card card)
        {
            StripeService stripe = new StripeService();

            PaymentToken paymentToken = stripe.CreateSourceToken(card);

            return paymentToken;
        }

        public Payment MakePayment(long paymentMethodId, Order order, bool capture)
        {
            PaymentMethod method = unitOfWork.PaymentMethods.Get(paymentMethodId);

            StripeService stripe = new StripeService();

            Payment payment = stripe.CreateCharge(order.Total, method.CustomerId, method.SourceCardId, capture);

            if (payment.Succeeded || payment.Pending)
            {
                order.Status = OrderStatus.Paid;
                order.PaymentMethodId = paymentMethodId;
                order.PaymentId = payment.Id;

                unitOfWork.Orders.Add(order);
                unitOfWork.Commit();
            }

            return payment;
        }

        public Payment MakePayment(string paymentTokenId, string creationTokenId, Order order, bool capture)
        {
            StripeService stripe = new StripeService();

            Payment payment = stripe.CreateCharge(order.Total, paymentTokenId, capture);

            if (payment.Succeeded || payment.Pending)
            {
                PaymentMethod method = CreatePaymentMethod(order.CustomerProfileId, creationTokenId);
                
                order.Status = OrderStatus.Paid;
                order.PaymentMethodId = method.Id;
                order.PaymentId = payment.Id;

                unitOfWork.Orders.Add(order);
                unitOfWork.Commit();
            }

            return payment;
        }
    }
}
