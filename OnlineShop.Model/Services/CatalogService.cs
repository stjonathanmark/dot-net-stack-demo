﻿using System;
using System.Collections.Generic;
using OnlineShop.Data;

namespace OnlineShop.Model.Services
{
    public class CatalogService : ICatalogService
    {
        private ICatalogUnitOfWork unitOfWork;

        public CatalogService(ICatalogUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            RepositoryInfo = new Dictionary<string, object>();
        }

        public IEnumerable<Category> GetAllCategories()
        {
            IEnumerable<Category> categories = unitOfWork.Categories.Get();

            return categories;
        }

        public IEnumerable<Product> GetAllProducts(int? skip = null, int? take = null)
        {
            ClearRepoInfo();
            IEnumerable<Product> products = unitOfWork.Products.GetWithCategories(skip, take);
            RepositoryInfo.Add("ProductsFound", unitOfWork.ProductsFound);
            return products;
        }

        public Category GetCategory(int categoryId)
        {
            Category category = unitOfWork.Categories.Get(categoryId);

            return category;
        }

        public Category GetCategoryByProduct(int productId)
        {
            Product product = unitOfWork.Products.GetWithCategory(productId);
            Category category = product.Category;

            return category;
        }

        public Product GetProduct(int productId)
        {
            //Later change to GetWithDetails - After creating Product Detail entity and table
            Product product = unitOfWork.Products.GetWithCategory(productId);

            return product;
        }

        public IEnumerable<Product> GetProductsByCategory(int categoryId, int? skip = null, int? take = null)
        {
            ClearRepoInfo();
            IEnumerable<Product> products = unitOfWork.Products.GetWithCategoriesByCategory(categoryId, skip, take);
            RepositoryInfo.Add("ProductsFound", unitOfWork.ProductsFound);
            return products;
        }

        public IEnumerable<Category> GetSubCategories(int categoryId)
        {
            IEnumerable<Category> categories = unitOfWork.Categories.GetByCategory(categoryId);

            return categories;
        }

        public IEnumerable<Product> SearchProducts(Func<Product, bool> predicate, int? skip = null, int? take = null)
        {
            ClearRepoInfo();
            IEnumerable<Product> products = unitOfWork.Products.Search(predicate, skip, take);
            RepositoryInfo.Add("ProductsFound", unitOfWork.ProductsFound);
            return products;
        }

        public Dictionary<string, object> RepositoryInfo { get; private set; }

        private void ClearRepoInfo()
        {
            RepositoryInfo.Clear();
        }
    }
}
