﻿using System.Collections.Generic;

namespace OnlineShop.Model.Services
{
    public interface ISecurityService
    {
        AccountCreationResult CreateAccount(User user, IEnumerable<int> roleIdsToAdd = null, IEnumerable<string> roleNamesToAdd = null);

        AuthenticationResult AuthenticateUser(string username, string password);

        User GetUserForTokenCreation(long userId);

        long GetUserIdByUsername(string username);

        Role GetRoleById(int roleId);

        IEnumerable<Role> GetRolesByIds(IEnumerable<int> roleIds);

        Role GetRoleByName(string roleName);

        IEnumerable<Role> GetRolesByNames(IEnumerable<string> roleNames);
    }
}
