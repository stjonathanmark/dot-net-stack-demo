﻿using System.Collections.Generic;
using OnlineShop.Data;

namespace OnlineShop.Model.Services
{
    public class GeneralService : IGeneralService
    {
        IGeneralUnitOfWork unitOfWork;

        public GeneralService(IGeneralUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public IEnumerable<State> GetAllStates()
        {
            IEnumerable<State> states = unitOfWork.States.Get();

            return states;
        }

        public IEnumerable<State> GetStatesByCountry(int countryId)
        {
            IEnumerable<State> states = unitOfWork.States.Search(s => s.CountryId == 1);

            return states;
        }
    }
}
