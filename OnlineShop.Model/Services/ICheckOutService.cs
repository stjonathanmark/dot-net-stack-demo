﻿using OnlineShop.Model.Payments;
using System.Collections.Generic;

namespace OnlineShop.Model.Services
{
    public interface ICheckOutService
    {
        IEnumerable<OrderedItem> GetOrderedItems(IEnumerable<Product> products, Dictionary<int, int> quantities, Dictionary<int, IEnumerable<int>> productOptions = null);

        PaymentMethod CreatePaymentMethod(long customerProfileId, string sourceCardId);

        PaymentToken GetPaymentToken(Card card);

        Order CreateOrder(long customerProfileId, Address shippingAddress, IEnumerable<OrderedItem> orderedItems);

        Payment MakePayment(long paymentMethodId, Order order, bool capture);

        Payment MakePayment(string paymentTokenId, string creationTokenId, Order order, bool capture);
    }
}
