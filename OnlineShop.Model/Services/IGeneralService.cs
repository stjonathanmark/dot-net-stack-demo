﻿using System.Collections.Generic;

namespace OnlineShop.Model.Services
{
    public interface IGeneralService
    {
        IEnumerable<State> GetAllStates();

        IEnumerable<State> GetStatesByCountry(int countryId);
    }
}
