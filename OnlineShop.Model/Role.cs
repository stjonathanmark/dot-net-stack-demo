﻿using System.Collections.Generic;
using OnlineShop.Data;

namespace OnlineShop.Model
{
    public class Role : BaseRole
    {
        public Role()
        {
            UserRoles = new List<UserRole>();
            RoleSubRoles = new List<RoleSubRole>();
        }

        public List<UserRole> UserRoles { get; set; }

        public List<RoleSubRole> RoleSubRoles { get; set; }
    }
}
