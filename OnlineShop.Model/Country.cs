﻿using System.Collections.Generic;

namespace OnlineShop.Model
{
    public class Country : BaseCountry
    {
        public Country()
        {
            States = new List<State>();
            Addresses = new List<Address>();
        }
        public List<State> States { get; set; }

        public List<Address> Addresses { get; set; }
    }
}
