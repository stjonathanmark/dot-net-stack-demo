﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsShipmentRatingOptions
    {
        public UpsShipmentRatingOptions()
        {

        }

        public string NegotiatedRatesIndicator { get; set; }
    }
}
