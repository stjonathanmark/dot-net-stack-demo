﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsShipper : UpsShipLocation
    {
        public UpsShipper()
        {

        }

        public string ShipperNumber { get; set; }
    }
}
