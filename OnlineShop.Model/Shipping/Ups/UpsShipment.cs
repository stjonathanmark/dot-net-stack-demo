﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsShipment
    {
        public UpsShipment()
        {
            Shipper = new UpsShipper();
            ShipTo = new UpsShipLocation();
            ShipFrom = new UpsShipLocation();
            Service = new UpsCode();
            Package = new UpsPackage();
            ShipmentRatingOptions = new UpsShipmentRatingOptions();
        }

        public UpsShipper Shipper { get; set; }

        public UpsShipLocation ShipTo { get; set; }

        public UpsShipLocation ShipFrom { get; set; }

        public UpsCode Service { get; set; }

        public UpsPackage Package { get; set; }

        public UpsShipmentRatingOptions ShipmentRatingOptions { get; set; }
    }
}
