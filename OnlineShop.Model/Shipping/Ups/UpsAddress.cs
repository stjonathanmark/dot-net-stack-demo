﻿
using System.Collections.Generic;

namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsAddress
    {
        public UpsAddress()
        {
            AddressLine = new List<string>();
        }

        public List<string> AddressLine { get; set; }

        public string City { get; set; }

        public string StateProvinceCode { get; set; }

        public string PostalCode { get; set; }

        public string CountryCode { get; set; }
    }
}
