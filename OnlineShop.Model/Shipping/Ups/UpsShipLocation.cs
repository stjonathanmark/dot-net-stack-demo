﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsShipLocation
    {
        public UpsShipLocation()
        {
            Address = new UpsAddress();
        }

        public string Name { get; set; }

        public UpsAddress Address { get; set; }
    }
}
