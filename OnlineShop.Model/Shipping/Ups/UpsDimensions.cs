﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsDimensions
    {
        public UpsDimensions()
        {
            UnitOfMeasurement = new UpsCode();
        }

        public UpsCode UnitOfMeasurement { get; set; }

        public string Length { get; set; }

        public string Width { get; set; }

        public string Height { get; set; }
    }
}
