﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsUsernameToken
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
