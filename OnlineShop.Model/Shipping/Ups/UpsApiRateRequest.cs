﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsApiRateRequest : UpsApiRequest
    {
        public UpsApiRateRequest()
            : base()
        {
            RateRequest = new UpsRateRequest();
        }

        public UpsRateRequest RateRequest { get; set; }
    }
}
