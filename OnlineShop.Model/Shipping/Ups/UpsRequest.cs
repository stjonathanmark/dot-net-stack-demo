﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsRequest
    {
        public UpsRequest()
        {
            TransactionReference = new UpsTransactionReference();
        }

        public string RequestOption { get; set; }

        public UpsTransactionReference TransactionReference { get; set; }
    }
}
