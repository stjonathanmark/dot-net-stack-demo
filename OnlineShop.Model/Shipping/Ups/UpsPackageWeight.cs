﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsPackageWeight
    {
        public UpsPackageWeight()
        {
            UnitOfMeasurement = new UpsCode();
        }

        public UpsCode UnitOfMeasurement { get; set; }

        public string Weight { get; set; }
    }
}
