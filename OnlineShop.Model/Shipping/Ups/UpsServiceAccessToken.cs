﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsServiceAccessToken
    {
        public string AccessLicenseNumber { get; set; }
    }
}
