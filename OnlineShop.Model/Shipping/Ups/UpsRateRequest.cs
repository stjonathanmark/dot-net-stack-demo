﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsRateRequest
    {
        public UpsRateRequest()
        {
            Request = new UpsRequest();
            Shipment = new UpsShipment();
        }

        public UpsRequest Request { get; set; }

        public UpsShipment Shipment { get; set; }
    }
}
