﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsSecurity
    {
        public UpsSecurity()
        {
            UsernameToken = new UpsUsernameToken();
            ServiceAccessToken = new UpsServiceAccessToken();
        }

        public UpsUsernameToken UsernameToken { get; set; }

        public UpsServiceAccessToken ServiceAccessToken { get; set; }
    }
}
