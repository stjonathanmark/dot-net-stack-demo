﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsCode
    {
        public UpsCode()
        {

        }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}
