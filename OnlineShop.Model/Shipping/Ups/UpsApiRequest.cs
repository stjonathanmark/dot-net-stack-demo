﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsApiRequest
    {
        public UpsApiRequest()
        {
            UpsSecurity = new UpsSecurity();
        }

        public UpsSecurity UpsSecurity { get; set; }
    }
}
