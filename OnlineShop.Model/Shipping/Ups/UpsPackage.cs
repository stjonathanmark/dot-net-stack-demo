﻿
namespace OnlineShop.Model.Shipping.Ups
{
    public class UpsPackage
    {
        public UpsPackage()
        {
            PackagingType = new UpsCode();
            Dimensions = new UpsDimensions();
            PackageWeight = new UpsPackageWeight();
        }

        public UpsCode PackagingType { get; set; }

        public UpsDimensions Dimensions { get; set; }

        public UpsPackageWeight PackageWeight { get; set; }
    }
}
