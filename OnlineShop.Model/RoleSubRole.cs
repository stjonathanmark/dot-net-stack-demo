﻿using OnlineShop.Model;

namespace OnlineShop.Model
{
    public class RoleSubRole : BaseRoleSubRole
    {
        public Role Role { get; set; }

        public Role SubRole { get; set; }
    }
}
