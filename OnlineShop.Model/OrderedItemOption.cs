﻿
namespace OnlineShop.Model
{
    public class OrderedItemOption : BaseOrderedItemOption
    {
        public OrderedItem OrderedItem { get; set; }
        
        public Option Option { get; set; }
    }
}
