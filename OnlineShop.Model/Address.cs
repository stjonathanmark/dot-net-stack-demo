﻿namespace OnlineShop.Model
{
    public class Address : BaseAddress
    {
        public State State { get; set; }

        public Country Country { get; set; }

        public Order Order { get; set; }

        public CustomerProfile CustomerProfile { get; set; }
    }
}
