﻿using System.Collections.Generic;

namespace OnlineShop.Model
{
    public class OptionSet : BaseOptionSet
    {
        public OptionSet()
        {
            Options = new List<Option>();
        }

        public Product Product { get; set; }

        public List<Option> Options { get; set; }
    }
}
