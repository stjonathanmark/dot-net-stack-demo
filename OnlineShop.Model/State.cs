﻿using System.Collections.Generic;

namespace OnlineShop.Model
{
    public class State : BaseState
    {
        public State()
        {
            Addresses = new List<Address>();
        }

        public Country Country { get; set; }

        public List<Address> Addresses { get; set; }
    }
}
