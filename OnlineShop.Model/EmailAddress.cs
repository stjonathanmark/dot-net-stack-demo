﻿
namespace OnlineShop.Model
{
    public class EmailAddress : BaseEmailAddress
    {
        public User User { get; set; }

        public CustomerProfile CustomerProfile { get; set; }
    }
}
