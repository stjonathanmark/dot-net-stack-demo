﻿using System;
using System.Collections.Generic;
using OnlineShop.Model.Payments;

namespace OnlineShop.Model
{
    public class Order : BaseOrder
    {
        public Order()
        {
            OrderedItems = new List<OrderedItem>();
        }

        public CustomerProfile CustomerProfile { get; set; }

        public List<OrderedItem> OrderedItems { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public Address ShippingAddress { get; set; }

        internal int TotalInCents
        {
            get
            {
                int cents = (int)(Total * 100);

                return cents;
            }
        }
    }
}
