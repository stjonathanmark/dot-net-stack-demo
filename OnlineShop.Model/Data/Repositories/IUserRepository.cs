﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface IUserRepository : IRepository<User, long>
    {
        User GetByUsername(string username);

        User GetForTokenCreation(long userId);

        long GetIdByUsername(string username);

        bool UsernameExists(string username);
    }
}
