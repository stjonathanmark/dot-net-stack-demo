﻿using OnlineShop.Model;
using System.Collections.Generic;

namespace OnlineShop.Data.Repositories
{
    public interface IStateRepository : IRepository<State, int>
    {
    }
}
