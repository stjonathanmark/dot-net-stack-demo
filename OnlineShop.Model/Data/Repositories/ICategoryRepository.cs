﻿using System.Collections.Generic;
using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface ICategoryRepository : IRepository<Category, int>
    {
        IEnumerable<Category> GetByCategory(int categoryId);
    }
}
