﻿using System.Collections.Generic;
using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface IProductRepository : IRepository<Product, int>
    {
        IEnumerable<Product> GetWithCategories(int? skip = null, int? take = null);

        IEnumerable<Product> GetWithCategoriesByCategory(int categoryId, int? skip = null, int? take = null);

        Product GetWithCategory(int productId);
    }
}
