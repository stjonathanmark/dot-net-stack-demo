﻿using System.Collections.Generic;
using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface IRoleRepository : IRepository<Role, int>
    {
        Role GetByName(string name);

        IEnumerable<Role> GetByNames(IEnumerable<string> names);
    }
}
