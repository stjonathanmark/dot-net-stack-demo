﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface IPhoneNumberRepository : IRepository<PhoneNumber, long>
    {
    }
}
