﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface IEmailAddressRepository : IRepository<EmailAddress, long>
    {
    }
}
