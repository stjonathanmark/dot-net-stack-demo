﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface IAddressRepository : IRepository<Address, long>
    {
    }
}
