﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface IPaymentMethodRepository : IRepository<PaymentMethod, long>
    {
    }
}
