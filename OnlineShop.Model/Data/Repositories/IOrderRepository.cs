﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface IOrderRepository : IRepository<Order, long>
    {
    }
}

