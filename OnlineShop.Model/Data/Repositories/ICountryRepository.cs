﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface ICountryRepository : IRepository<Country, int>
    {
    }
}
