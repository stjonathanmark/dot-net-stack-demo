﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface IOptionRepository : IRepository<Option, int>
    {
    }
}

