﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface IOptionSetRepository : IRepository<OptionSet, int>
    {
    }
}

