﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface IPasswordRepository : IRepository<Password, long>
    {
    }
}
