﻿using System;
using System.Collections.Generic;

namespace OnlineShop.Data.Repositories
{
    public interface IRepository<TEntity, TEntityId>
        where TEntityId : struct
        where TEntity : BaseEntity<TEntityId>
    {
        void Add(TEntity entity);

        void Add(IEnumerable<TEntity> entities);

        TEntity Get(TEntityId id);

        IEnumerable<TEntity> Get(IEnumerable<TEntityId> ids);

        IEnumerable<TEntity> Get(int? skip = null, int? take = null);

        IEnumerable<TEntity> Search(Func<TEntity, bool> predicate, int? skip = null, int? take = null);

        void Remove(TEntity entity);

        void Remove(IEnumerable<TEntity> entities);

        void Update(TEntity entity);

        void Update(IEnumerable<TEntity> entities);

        Dictionary<string, Object> RepositoryInfo { get; }

        string BaseKey { get; }
    }
}
