﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface IOrderedItemRepository : IRepository<OrderedItem, long>
    {
    }
}

