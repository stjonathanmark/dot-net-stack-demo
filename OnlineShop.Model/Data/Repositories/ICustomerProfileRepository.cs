﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public interface ICustomerProfileRepository : IRepository<CustomerProfile, long>
    {
        CustomerProfile GetWithContactInformation(long customerProfileId);
    }
}
