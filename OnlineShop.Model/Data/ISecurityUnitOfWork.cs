﻿using OnlineShop.Data.Repositories;

namespace OnlineShop.Data
{
    public interface ISecurityUnitOfWork : IUnitOfWork
    {
        IUserRepository Users { get; set; }

        IRoleRepository Roles { get; set; }

        IPasswordRepository Passwords { get; set; }
    }
}
