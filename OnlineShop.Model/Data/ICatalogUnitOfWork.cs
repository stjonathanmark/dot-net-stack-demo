﻿using OnlineShop.Data.Repositories;

namespace OnlineShop.Data
{
    public interface ICatalogUnitOfWork : IUnitOfWork
    {
        ICategoryRepository Categories { get; }

        IProductRepository Products { get; }

        int? CategoriesFound { get; }

        int? ProductsFound { get; }
    }
}
