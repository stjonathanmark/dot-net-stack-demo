﻿using OnlineShop.Data.Repositories;

namespace OnlineShop.Data
{
    public interface IGeneralUnitOfWork
    {
        IStateRepository States { get; set; }

        ICountryRepository Countries { get; set; }
    }
}
