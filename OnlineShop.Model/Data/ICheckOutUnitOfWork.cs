﻿using OnlineShop.Data.Repositories;

namespace OnlineShop.Data
{
    public interface ICheckOutUnitOfWork : IUnitOfWork
    {
        ICustomerProfileRepository CustomerProfiles { get; set; }

        IPaymentMethodRepository PaymentMethods { get; set; }

        IOrderRepository Orders { get; set; }
    }
}
