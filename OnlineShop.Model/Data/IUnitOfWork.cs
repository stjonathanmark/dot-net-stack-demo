﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShop.Data
{
    public interface IUnitOfWork
    {
        void Commit();

        Dictionary<string, Object> UnitOfWorkInfo { get; }
    }
}
