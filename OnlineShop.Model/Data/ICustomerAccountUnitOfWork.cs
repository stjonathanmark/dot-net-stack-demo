﻿using OnlineShop.Data.Repositories;

namespace OnlineShop.Data
{
    public interface ICustomerAccountUnitOfWork : IUnitOfWork
    {
        IUserRepository Users { get; set; }
    }
}
