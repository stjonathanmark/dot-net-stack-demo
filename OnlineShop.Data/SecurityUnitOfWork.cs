﻿using OnlineShop.Data.Repositories;

namespace OnlineShop.Data
{
    public class SecurityUnitOfWork : UnitOfWork, ISecurityUnitOfWork
    {
        public SecurityUnitOfWork(DataSource context)
            : base(context)
        {
            Users = new UserRepository(context);
            Roles = new RoleRepository(context);
            Passwords = new PasswordRepository(context);
        }

        public IUserRepository Users { get; set; }

        public IRoleRepository Roles { get; set; }

        public IPasswordRepository Passwords { get; set; }
    }
}
