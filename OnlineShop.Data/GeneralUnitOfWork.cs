﻿using OnlineShop.Data.Repositories;

namespace OnlineShop.Data
{
    public class GeneralUnitOfWork : UnitOfWork, IGeneralUnitOfWork
    {
        public GeneralUnitOfWork(DataSource context)
            : base(context)
        {
            States = new StateRepository(context);
            Countries = new CountryRepository(context);
        }

        public IStateRepository States { get; set; }

        public ICountryRepository Countries { get; set; }
    }
}
