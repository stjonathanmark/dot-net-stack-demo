﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineShop.Data.Migrations
{
    public partial class InitialModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "Categories",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    ParentCategoryId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Categories_Categories_ParentCategoryId",
                        column: x => x.ParentCategoryId,
                        principalSchema: "dbo",
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    Abbreviation = table.Column<string>(maxLength: 5, nullable: false),
                    Name = table.Column<string>(maxLength: 75, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    Username = table.Column<string>(maxLength: 50, nullable: false),
                    Password = table.Column<string>(maxLength: 1024, nullable: false),
                    Activated = table.Column<bool>(nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    LockedOut = table.Column<bool>(nullable: false),
                    PasswordIsTemporary = table.Column<bool>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    LastPasswordChangeDate = table.Column<DateTime>(nullable: false),
                    FailedLoginAttempts = table.Column<int>(nullable: false),
                    Salt = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 150, nullable: false),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Weight = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    InStock = table.Column<bool>(nullable: false),
                    ImageUrl = table.Column<string>(maxLength: 1024, nullable: false),
                    ThumbnailImageUrl = table.Column<string>(maxLength: 1024, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalSchema: "dbo",
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "States",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    CountryId = table.Column<int>(nullable: false),
                    Abbreviation = table.Column<string>(maxLength: 5, nullable: false),
                    Name = table.Column<string>(maxLength: 75, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_States", x => x.Id);
                    table.ForeignKey(
                        name: "FK_States_Countries_CountryId",
                        column: x => x.CountryId,
                        principalSchema: "dbo",
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RoleSubRoles",
                schema: "dbo",
                columns: table => new
                {
                    RoleId = table.Column<int>(nullable: false),
                    SubRoleId = table.Column<int>(nullable: false),
                    RoleId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleSubRoles", x => new { x.RoleId, x.SubRoleId });
                    table.ForeignKey(
                        name: "FK_RoleSubRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "dbo",
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RoleSubRoles_Roles_RoleId1",
                        column: x => x.RoleId1,
                        principalSchema: "dbo",
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RoleSubRoles_Roles_SubRoleId",
                        column: x => x.SubRoleId,
                        principalSchema: "dbo",
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CustomerProfiles",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    IsPersonal = table.Column<bool>(nullable: false),
                    CompanyName = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 50, nullable: false),
                    PaymentGatewayId = table.Column<string>(maxLength: 75, nullable: true),
                    BirthDate = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerProfiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomerProfiles_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "dbo",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Passwords",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Temporary = table.Column<bool>(nullable: false),
                    Secret = table.Column<string>(maxLength: 1024, nullable: false),
                    Salt = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Passwords", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Passwords_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "dbo",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                schema: "dbo",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "dbo",
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "dbo",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OptionSets",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    ProductId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(maxLength: 75, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OptionSets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OptionSets_Products_ProductId",
                        column: x => x.ProductId,
                        principalSchema: "dbo",
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmailAddresses",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    UserId = table.Column<long>(nullable: true),
                    CustomerProfileId = table.Column<long>(nullable: true),
                    IsPrimary = table.Column<bool>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Address = table.Column<string>(maxLength: 75, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailAddresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailAddresses_CustomerProfiles_CustomerProfileId",
                        column: x => x.CustomerProfileId,
                        principalSchema: "dbo",
                        principalTable: "CustomerProfiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmailAddresses_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "dbo",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PaymentMethods",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    CustomerProfileId = table.Column<long>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    CustomerId = table.Column<string>(maxLength: 75, nullable: false),
                    SourceCardId = table.Column<string>(maxLength: 75, nullable: false),
                    CardType = table.Column<string>(maxLength: 16, nullable: false),
                    ExpirationDate = table.Column<DateTime>(nullable: false),
                    LastFourDigits = table.Column<string>(maxLength: 4, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMethods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentMethods_CustomerProfiles_CustomerProfileId",
                        column: x => x.CustomerProfileId,
                        principalSchema: "dbo",
                        principalTable: "CustomerProfiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PhoneNumbers",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    CustomerProfileId = table.Column<long>(nullable: false),
                    IsPrimary = table.Column<bool>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Number = table.Column<string>(maxLength: 25, nullable: false),
                    Extension = table.Column<string>(maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoneNumbers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhoneNumbers_CustomerProfiles_CustomerProfileId",
                        column: x => x.CustomerProfileId,
                        principalSchema: "dbo",
                        principalTable: "CustomerProfiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Options",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    OptionSetId = table.Column<int>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 75, nullable: false),
                    Value = table.Column<string>(maxLength: 75, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Options", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Options_OptionSets_OptionSetId",
                        column: x => x.OptionSetId,
                        principalSchema: "dbo",
                        principalTable: "OptionSets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    CustomerProfileId = table.Column<long>(nullable: false),
                    PaymentMethodId = table.Column<long>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    SubTotal = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Tax = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ShippingCost = table.Column<decimal>(nullable: false),
                    Total = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PaymentId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_CustomerProfiles_CustomerProfileId",
                        column: x => x.CustomerProfileId,
                        principalSchema: "dbo",
                        principalTable: "CustomerProfiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_PaymentMethods_PaymentMethodId",
                        column: x => x.PaymentMethodId,
                        principalSchema: "dbo",
                        principalTable: "PaymentMethods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    CustomerProfileId = table.Column<long>(nullable: true),
                    OrderId = table.Column<long>(nullable: true),
                    PaymentMethodId = table.Column<long>(nullable: true),
                    StateId = table.Column<int>(nullable: false),
                    CountryId = table.Column<int>(nullable: false),
                    IsPrimary = table.Column<bool>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    StreetOne = table.Column<string>(maxLength: 150, nullable: false),
                    StreetTwo = table.Column<string>(maxLength: 100, nullable: false),
                    City = table.Column<string>(maxLength: 100, nullable: false),
                    ZipCode = table.Column<string>(maxLength: 25, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Addresses_Countries_CountryId",
                        column: x => x.CountryId,
                        principalSchema: "dbo",
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_CustomerProfiles_CustomerProfileId",
                        column: x => x.CustomerProfileId,
                        principalSchema: "dbo",
                        principalTable: "CustomerProfiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_Orders_OrderId",
                        column: x => x.OrderId,
                        principalSchema: "dbo",
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_States_StateId",
                        column: x => x.StateId,
                        principalSchema: "dbo",
                        principalTable: "States",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderedItems",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deleted = table.Column<bool>(nullable: false),
                    OrderId = table.Column<long>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderedItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderedItems_Orders_OrderId",
                        column: x => x.OrderId,
                        principalSchema: "dbo",
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderedItems_Products_ProductId",
                        column: x => x.ProductId,
                        principalSchema: "dbo",
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderedItemOptions",
                schema: "dbo",
                columns: table => new
                {
                    OrderedItemId = table.Column<long>(nullable: false),
                    OptionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderedItemOptions", x => new { x.OrderedItemId, x.OptionId });
                    table.ForeignKey(
                        name: "FK_OrderedItemOptions_Options_OptionId",
                        column: x => x.OptionId,
                        principalSchema: "dbo",
                        principalTable: "Options",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderedItemOptions_OrderedItems_OrderedItemId",
                        column: x => x.OrderedItemId,
                        principalSchema: "dbo",
                        principalTable: "OrderedItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Categories",
                columns: new[] { "Id", "Deleted", "Description", "Name", "ParentCategoryId" },
                values: new object[,]
                {
                    { 1, false, null, "Electronics", null },
                    { 2, false, null, "Apparel", null },
                    { 3, false, null, "Food", null },
                    { 4, false, null, "Furniture", null },
                    { 5, false, null, "Appliances", null }
                });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Countries",
                columns: new[] { "Id", "Abbreviation", "Deleted", "Name" },
                values: new object[] { 1, "US", false, "United States" });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Deleted", "Description", "ImageUrl", "InStock", "Name", "Price", "ThumbnailImageUrl", "Weight" },
                values: new object[,]
                {
                    { 1, 1, false, null, "product-image.jpg", false, "Laptop", 1100m, "product-thumbnail.jpg", 8m },
                    { 21, 3, false, null, "product-image.jpg", false, "Potato Chips", 1.6m, "product-thumbnail.jpg", 1m },
                    { 22, 3, false, null, "product-image.jpg", false, "Grapes", 3m, "product-thumbnail.jpg", 1m },
                    { 23, 3, false, null, "product-image.jpg", false, "Orange Juice", 4m, "product-thumbnail.jpg", 1m },
                    { 24, 3, false, null, "product-image.jpg", false, "Milk", 3.15m, "product-thumbnail.jpg", 1m },
                    { 25, 3, false, null, "product-image.jpg", false, "Bottled Water 24 Pack", 2.5m, "product-thumbnail.jpg", 1m },
                    { 26, 3, false, null, "product-image.jpg", false, "Cookies", 4m, "product-thumbnail.jpg", 1m },
                    { 27, 3, false, null, "product-image.jpg", false, "Ground Beef", 6.4m, "product-thumbnail.jpg", 1m },
                    { 28, 4, false, null, "product-image.jpg", false, "Bed", 800m, "product-thumbnail.jpg", 300m },
                    { 29, 4, false, null, "product-image.jpg", false, "Desk", 450m, "product-thumbnail.jpg", 50m },
                    { 30, 4, false, null, "product-image.jpg", false, "Sofa", 500m, "product-thumbnail.jpg", 75m },
                    { 32, 4, false, null, "product-image.jpg", false, "Chair", 50m, "product-thumbnail.jpg", 15m },
                    { 33, 5, false, null, "product-image.jpg", false, "Refrigerator", 1500m, "product-thumbnail.jpg", 400m },
                    { 34, 5, false, null, "product-image.jpg", false, "Washer", 1200m, "product-thumbnail.jpg", 200m },
                    { 35, 5, false, null, "product-image.jpg", false, "Dryer", 1200m, "product-thumbnail.jpg", 200m },
                    { 36, 5, false, null, "product-image.jpg", false, "Microwave", 150m, "product-thumbnail.jpg", 10m },
                    { 20, 3, false, null, "product-image.jpg", false, "Cereal", 4m, "product-thumbnail.jpg", 1m },
                    { 19, 3, false, null, "product-image.jpg", false, "Bread", 2.75m, "product-thumbnail.jpg", 1m },
                    { 31, 4, false, null, "product-image.jpg", false, "Table", 2450m, "product-thumbnail.jpg", 25m },
                    { 17, 2, false, null, "product-image.jpg", false, "Socks", 2.5m, "product-thumbnail.jpg", 1m },
                    { 18, 2, false, null, "product-image.jpg", false, "Tennis Shoes", 50m, "product-thumbnail.jpg", 1m },
                    { 2, 1, false, null, "product-image.jpg", false, "IPhone", 300m, "product-thumbnail.jpg", 0.7m },
                    { 3, 1, false, null, "product-image.jpg", false, "Galaxy Phone", 250m, "product-thumbnail.jpg", 0.8m },
                    { 4, 1, false, null, "product-image.jpg", false, "Android Phone", 175m, "product-thumbnail.jpg", 0.76m },
                    { 6, 1, false, null, "product-image.jpg", false, "DVR", 800m, "product-thumbnail.jpg", 4m },
                    { 7, 1, false, null, "product-image.jpg", false, "Desktop", 1200m, "product-thumbnail.jpg", 20m },
                    { 8, 1, false, null, "product-image.jpg", false, "Blu-Ray Player", 80m, "product-thumbnail.jpg", 2m },
                    { 5, 1, false, null, "product-image.jpg", false, "Printer", 200m, "product-thumbnail.jpg", 9m },
                    { 10, 2, false, null, "product-image.jpg", false, "Polo Shirt", 11m, "product-thumbnail.jpg", 1m },
                    { 11, 2, false, null, "product-image.jpg", false, "Jeans", 9m, "product-thumbnail.jpg", 1m },
                    { 12, 2, false, null, "product-image.jpg", false, "Khaki Pants", 12m, "product-thumbnail.jpg", 1m },
                    { 13, 2, false, null, "product-image.jpg", false, "T-Shirt", 2m, "product-thumbnail.jpg", 1m },
                    { 14, 2, false, null, "product-image.jpg", false, "Sweater", 5m, "product-thumbnail.jpg", 1m },
                    { 15, 2, false, null, "product-image.jpg", false, "Jogging Pants", 8m, "product-thumbnail.jpg", 1m },
                    { 9, 1, false, null, "product-image.jpg", false, "50' LED HDTV", 500m, "product-thumbnail.jpg", 15m },
                    { 16, 2, false, null, "product-image.jpg", false, "Shorts", 3m, "product-thumbnail.jpg", 1m }
                });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "States",
                columns: new[] { "Id", "Abbreviation", "CountryId", "Deleted", "Name" },
                values: new object[,]
                {
                    { 36, "OH", 1, false, "Ohio" },
                    { 35, "ND", 1, false, "North Dakota" },
                    { 34, "NC", 1, false, "North Carolina" },
                    { 33, "NY", 1, false, "New York" },
                    { 30, "NH", 1, false, "New Hampshire" },
                    { 31, "NJ", 1, false, "New Jersey" },
                    { 29, "NV", 1, false, "Nevada" },
                    { 37, "OK", 1, false, "Oklahoma" },
                    { 28, "NE", 1, false, "Nebraska" },
                    { 32, "NM", 1, false, "New Mexico" },
                    { 38, "OR", 1, false, "Oregon" },
                    { 47, "MT", 1, false, "Vermont" },
                    { 40, "PR", 1, false, "Puerto Rico" },
                    { 41, "RI", 1, false, "Rhode Island" },
                    { 42, "SC", 1, false, "South Carolina" },
                    { 43, "SD", 1, false, "South Dakota" },
                    { 44, "TN", 1, false, "Tennessee" },
                    { 45, "TX", 1, false, "Texas" },
                    { 46, "UT", 1, false, "Utah" },
                    { 48, "VA", 1, false, "Virginia" },
                    { 49, "WA", 1, false, "Washington" },
                    { 50, "WV", 1, false, "West Virginia" },
                    { 27, "MT", 1, false, "Montana" },
                    { 39, "PA", 1, false, "Pennsylvania" },
                    { 26, "MO", 1, false, "Missouri" },
                    { 8, "DE", 1, false, "Delaware" },
                    { 24, "MN", 1, false, "Minnesota" },
                    { 1, "AL", 1, false, "Alabama" },
                    { 2, "AK", 1, false, "Alaska" },
                    { 3, "AZ", 1, false, "Arizona" },
                    { 4, "AR", 1, false, "Arkansas" },
                    { 5, "CA", 1, false, "California" },
                    { 6, "CO", 1, false, "Colorado" },
                    { 7, "CT", 1, false, "Connecticut" },
                    { 51, "WI", 1, false, "Wisconsin" },
                    { 9, "DC", 1, false, "District of Columbia" },
                    { 10, "FL", 1, false, "Florida" },
                    { 11, "GA", 1, false, "Georgia" },
                    { 12, "HI", 1, false, "Hawaii" },
                    { 13, "ID", 1, false, "Idaho" },
                    { 14, "IL", 1, false, "Illinois" },
                    { 15, "IN", 1, false, "Indiana" },
                    { 16, "IA", 1, false, "Iowa" },
                    { 17, "KS", 1, false, "Kansas" },
                    { 18, "KY", 1, false, "Kentucky" },
                    { 19, "LA", 1, false, "Louisiana" },
                    { 20, "ME", 1, false, "Maine" },
                    { 21, "MD", 1, false, "Maryland" },
                    { 22, "MA", 1, false, "Massachusetts" },
                    { 23, "MI", 1, false, "Michigan" },
                    { 25, "MS", 1, false, "Mississippi" },
                    { 52, "WY", 1, false, "Wyoming" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_CountryId",
                schema: "dbo",
                table: "Addresses",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_CustomerProfileId",
                schema: "dbo",
                table: "Addresses",
                column: "CustomerProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_OrderId",
                schema: "dbo",
                table: "Addresses",
                column: "OrderId",
                unique: true,
                filter: "[OrderId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_StateId",
                schema: "dbo",
                table: "Addresses",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_ParentCategoryId",
                schema: "dbo",
                table: "Categories",
                column: "ParentCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerProfiles_UserId",
                schema: "dbo",
                table: "CustomerProfiles",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EmailAddresses_CustomerProfileId",
                schema: "dbo",
                table: "EmailAddresses",
                column: "CustomerProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailAddresses_UserId",
                schema: "dbo",
                table: "EmailAddresses",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Options_OptionSetId",
                schema: "dbo",
                table: "Options",
                column: "OptionSetId");

            migrationBuilder.CreateIndex(
                name: "IX_OptionSets_ProductId",
                schema: "dbo",
                table: "OptionSets",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderedItemOptions_OptionId",
                schema: "dbo",
                table: "OrderedItemOptions",
                column: "OptionId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderedItems_OrderId",
                schema: "dbo",
                table: "OrderedItems",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderedItems_ProductId",
                schema: "dbo",
                table: "OrderedItems",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CustomerProfileId",
                schema: "dbo",
                table: "Orders",
                column: "CustomerProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_PaymentMethodId",
                schema: "dbo",
                table: "Orders",
                column: "PaymentMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_Passwords_UserId",
                schema: "dbo",
                table: "Passwords",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentMethods_CustomerProfileId",
                schema: "dbo",
                table: "PaymentMethods",
                column: "CustomerProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_PhoneNumbers_CustomerProfileId",
                schema: "dbo",
                table: "PhoneNumbers",
                column: "CustomerProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                schema: "dbo",
                table: "Products",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleSubRoles_RoleId1",
                schema: "dbo",
                table: "RoleSubRoles",
                column: "RoleId1");

            migrationBuilder.CreateIndex(
                name: "IX_RoleSubRoles_SubRoleId",
                schema: "dbo",
                table: "RoleSubRoles",
                column: "SubRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_States_CountryId",
                schema: "dbo",
                table: "States",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleId",
                schema: "dbo",
                table: "UserRoles",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Addresses",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "EmailAddresses",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "OrderedItemOptions",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Passwords",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "PhoneNumbers",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "RoleSubRoles",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "UserRoles",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "States",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Options",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "OrderedItems",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Roles",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Countries",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "OptionSets",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Orders",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Products",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "PaymentMethods",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Categories",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "CustomerProfiles",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "dbo");
        }
    }
}
