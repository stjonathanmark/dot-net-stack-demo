﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineShop.Model;
using OnlineShop.Model.Payments;

namespace OnlineShop.Data
{
    internal static class DataMapper
    {

        public static void Map(EntityTypeBuilder<Address> entity, string tableName = "Addresses", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);

            entity.Property(a => a.Name).HasMaxLength(100).IsRequired();
            entity.Property(a => a.StreetOne).HasMaxLength(150).IsRequired();
            entity.Property(a => a.StreetTwo).HasMaxLength(100).IsRequired();
            entity.Property(a => a.City).HasMaxLength(100).IsRequired();
            entity.Property(a => a.ZipCode).HasMaxLength(25).IsRequired();

            entity.HasOne(a => a.State).WithMany(s => s.Addresses).HasForeignKey(a => a.StateId).OnDelete(DeleteBehavior.Restrict);
            entity.HasOne(a => a.Country).WithMany(c => c.Addresses).HasForeignKey(a => a.CountryId).OnDelete(DeleteBehavior.Restrict);
        }

        public static void Map(EntityTypeBuilder<Category> entity, string tableName = "Categories", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(c => c.Id);

            entity.Property(c => c.Name).HasMaxLength(50).IsRequired();
            entity.Property(c => c.Description).HasMaxLength(150);

            entity.HasMany(c => c.SubCategories).WithOne(sc => sc.ParentCategory).HasForeignKey(sc => sc.ParentCategoryId).OnDelete(DeleteBehavior.Restrict);
            entity.HasMany(c => c.Products).WithOne(p => p.Category).HasForeignKey(p => p.CategoryId).OnDelete(DeleteBehavior.Restrict);

            entity.HasData(new Category[]
            {
                new Category { Id = 1, Name = "Electronics" },
                new Category { Id = 2, Name = "Apparel" },
                new Category { Id = 3, Name = "Food" },
                new Category { Id = 4, Name = "Furniture" },
                new Category { Id = 5, Name = "Appliances" }
            });
        }

        public static void Map(EntityTypeBuilder<Country> entity, string tableName = "Countries", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);

            entity.Property(s => s.Abbreviation).HasMaxLength(5).IsRequired();
            entity.Property(s => s.Name).HasMaxLength(75).IsRequired();

            entity.HasMany(c => c.States).WithOne(s => s.Country).HasForeignKey(s => s.CountryId).OnDelete(DeleteBehavior.Restrict);

            entity.HasData(new Country { Id = 1, Name = "United States", Abbreviation = "US" });
        }

        public static void Map(EntityTypeBuilder<CustomerProfile> entity, string tableName = "CustomerProfiles", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);

            entity.Property(cp => cp.FirstName).HasMaxLength(50).IsRequired();
            entity.Property(cp => cp.LastName).HasMaxLength(50).IsRequired();
            entity.Property(cp => cp.BirthDate).HasColumnType("date");
            entity.Property(cp => cp.PaymentGatewayId).HasMaxLength(75);

            entity.HasMany(cp => cp.Addresses).WithOne(a => a.CustomerProfile).HasForeignKey(a => a.CustomerProfileId).OnDelete(DeleteBehavior.Restrict);
            entity.HasMany(cp => cp.EmailAddresses).WithOne(ea => ea.CustomerProfile).HasForeignKey(ea => ea.CustomerProfileId).OnDelete(DeleteBehavior.Restrict);
            entity.HasMany(cp => cp.PhoneNumbers).WithOne(pn => pn.CustomerProfile).HasForeignKey(p => p.CustomerProfileId).OnDelete(DeleteBehavior.Restrict);
            entity.HasMany(cp => cp.PaymentMethods).WithOne(pm => pm.CustomerProfile).HasForeignKey(p => p.CustomerProfileId).OnDelete(DeleteBehavior.Restrict);
        }

        public static void Map(EntityTypeBuilder<EmailAddress> entity, string tableName = "EmailAddresses", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);

            entity.Property(ea => ea.Address).HasMaxLength(75).IsRequired();
        }

        public static void Map(EntityTypeBuilder<Option> entity, string tableName = "Options", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);

            entity.Property(o => o.Name).HasMaxLength(75).IsRequired();
            entity.Property(o => o.Value).HasMaxLength(75).IsRequired();

            entity.HasMany(o => o.OrderedItemOptions).WithOne(oio => oio.Option).HasForeignKey(oio => oio.OptionId).OnDelete(DeleteBehavior.Restrict);
        }

        public static void Map(EntityTypeBuilder<OptionSet> entity, string tableName = "OptionSets", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);

            entity.Property(os => os.Name).HasMaxLength(75).IsRequired();

            entity.HasMany(os => os.Options).WithOne(o => o.OptionSet).HasForeignKey(o => o.OptionSetId).OnDelete(DeleteBehavior.Restrict);
        }

        public static void Map(EntityTypeBuilder<Order> entity, string tableName = "Orders", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);
            
            entity.Property(o => o.SubTotal).HasColumnType("decimal(18,2)").IsRequired();
            entity.Property(o => o.Tax).HasColumnType("decimal(18,2)").IsRequired();
            entity.Property(o => o.Total).HasColumnType("decimal(18,2)").IsRequired();

            entity.HasMany(o => o.OrderedItems).WithOne(oi => oi.Order).HasForeignKey(oi => oi.OrderId).OnDelete(DeleteBehavior.Restrict);
            entity.HasOne(o => o.ShippingAddress).WithOne(a => a.Order).HasForeignKey<Address>(a => a.OrderId).OnDelete(DeleteBehavior.Restrict);
        }

        public static void Map(EntityTypeBuilder<OrderedItem> entity, string tableName = "OrderedItems", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);

            entity.Property(o => o.Price).HasColumnType("decimal(18,2)").IsRequired();

            entity.HasMany(oi => oi.OrderedItemOptions).WithOne(oio => oio.OrderedItem).HasForeignKey(oio => oio.OrderedItemId).OnDelete(DeleteBehavior.Restrict);
        }

        public static void Map(EntityTypeBuilder<OrderedItemOption> entity, string tableName = "OrderedItemOptions", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => new { e.OrderedItemId, e.OptionId });
        }

        public static void Map(EntityTypeBuilder<Password> entity, string tableName = "Passwords", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);

            entity.Property(p => p.Secret).HasMaxLength(1024).IsRequired();
            entity.Property(p => p.Salt).HasMaxLength(50).IsRequired();
        }

        public static void Map(EntityTypeBuilder<PaymentMethod> entity, string tableName = "PaymentMethods", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);

            entity.Property(pm => pm.CustomerId).HasMaxLength(75).IsRequired();
            entity.Property(pm => pm.SourceCardId).HasMaxLength(75).IsRequired();
            entity.Property(pm => pm.CardType).HasMaxLength(16).IsRequired();
            entity.Property(pm => pm.LastFourDigits).HasMaxLength(4).IsRequired();
            entity.Ignore(pm => pm.IsExpired);
            entity.Ignore(pm => pm.FormattedExpirationDate);

            entity.HasMany(pm => pm.Orders).WithOne(o => o.PaymentMethod).HasForeignKey(o => o.PaymentMethodId).OnDelete(DeleteBehavior.Restrict);
        }

        public static void Map(EntityTypeBuilder<PhoneNumber> entity, string tableName = "PhoneNumbers", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);

            entity.Property(p => p.Number).HasMaxLength(25).IsRequired();
            entity.Property(p => p.Extension).HasMaxLength(15).IsRequired();
        }

        public static void Map(EntityTypeBuilder<Product> entity, string tableName = "Products", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(p => p.Id);

            entity.Property(p => p.Name).HasMaxLength(150).IsRequired();
            entity.Property(p => p.Description).HasMaxLength(200);
            entity.Property(p => p.ImageUrl).HasMaxLength(1024).IsRequired();
            entity.Property(p => p.ThumbnailImageUrl).HasMaxLength(1024).IsRequired();
            entity.Property(p => p.Weight).HasColumnType("decimal(18,2)").IsRequired();
            entity.Property(p => p.Price).HasColumnType("decimal(18,2)").IsRequired();

            entity.HasMany(p => p.OptionSets).WithOne(os => os.Product).HasForeignKey(os => os.ProductId).OnDelete(DeleteBehavior.Restrict);
            entity.HasMany(p => p.OrderedItems).WithOne(oi => oi.Product).HasForeignKey(oi => oi.ProductId).OnDelete(DeleteBehavior.Restrict);

            entity.HasData(new Product[]
            {
                new Product { Id = 1, CategoryId = 1, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Laptop", Price = 1100M,  Weight = 8M },
                new Product { Id = 2, CategoryId = 1, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "IPhone", Price = 300M,  Weight = 0.7M },
                new Product { Id = 3, CategoryId = 1, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Galaxy Phone", Price = 250M,  Weight = 0.8M },
                new Product { Id = 4, CategoryId = 1, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Android Phone", Price = 175M,  Weight = 0.76M },
                new Product { Id = 5, CategoryId = 1, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Printer", Price = 200M,  Weight = 9M },
                new Product { Id = 6, CategoryId = 1, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "DVR", Price = 800M,  Weight = 4M },
                new Product { Id = 7, CategoryId = 1, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Desktop", Price = 1200M,  Weight = 20M },
                new Product { Id = 8, CategoryId = 1, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Blu-Ray Player", Price = 80M,  Weight = 2M },
                new Product { Id = 9, CategoryId = 1, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "50' LED HDTV", Price = 500M,  Weight = 15M },
                new Product { Id = 10, CategoryId = 2, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Polo Shirt", Price = 11M,  Weight = 1M },
                new Product { Id = 11, CategoryId = 2, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Jeans", Price = 9M,  Weight = 1M },
                new Product { Id = 12, CategoryId = 2, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Khaki Pants", Price = 12M,  Weight = 1M },
                new Product { Id = 13, CategoryId = 2, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "T-Shirt", Price = 2M,  Weight = 1M },
                new Product { Id = 14, CategoryId = 2, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Sweater", Price = 5M,  Weight = 1M },
                new Product { Id = 15, CategoryId = 2, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Jogging Pants", Price = 8M,  Weight = 1M },
                new Product { Id = 16, CategoryId = 2, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Shorts", Price = 3M,  Weight = 1M },
                new Product { Id = 17, CategoryId = 2, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Socks", Price = 2.5M,  Weight = 1M },
                new Product { Id = 18, CategoryId = 2, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Tennis Shoes", Price = 50M,  Weight = 1M },
                new Product { Id = 19, CategoryId = 3, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Bread", Price = 2.75M,  Weight = 1M },
                new Product { Id = 20, CategoryId = 3, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Cereal", Price = 4M,  Weight = 1M },
                new Product { Id = 21, CategoryId = 3, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Potato Chips", Price = 1.6M,  Weight = 1M },
                new Product { Id = 22, CategoryId = 3, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Grapes", Price = 3M,  Weight = 1M },
                new Product { Id = 23, CategoryId = 3, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Orange Juice", Price = 4M,  Weight = 1M },
                new Product { Id = 24, CategoryId = 3, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Milk", Price = 3.15M,  Weight = 1M },
                new Product { Id = 25, CategoryId = 3, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Bottled Water 24 Pack", Price = 2.5M,  Weight = 1M },
                new Product { Id = 26, CategoryId = 3, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Cookies", Price = 4M,  Weight = 1M },
                new Product { Id = 27, CategoryId = 3, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Ground Beef", Price = 6.4M,  Weight = 1M },
                new Product { Id = 28, CategoryId = 4, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Bed", Price = 800M,  Weight = 300M },
                new Product { Id = 29, CategoryId = 4, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Desk", Price = 450M,  Weight = 50M },
                new Product { Id = 30, CategoryId = 4, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Sofa", Price = 500M,  Weight = 75M },
                new Product { Id = 31, CategoryId = 4, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Table", Price = 2450M,  Weight = 25M },
                new Product { Id = 32, CategoryId = 4, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Chair", Price = 50M,  Weight = 15M },
                new Product { Id = 33, CategoryId = 5, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Refrigerator", Price = 1500M,  Weight = 400M },
                new Product { Id = 34, CategoryId = 5, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Washer", Price = 1200M,  Weight = 200M },
                new Product { Id = 35, CategoryId = 5, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Dryer", Price = 1200M,  Weight = 200M },
                new Product { Id = 36, CategoryId = 5, ImageUrl = "product-image.jpg", ThumbnailImageUrl = "product-thumbnail.jpg", Name = "Microwave", Price = 150M,  Weight = 10M }
            });

        }

        public static void Map(EntityTypeBuilder<Role> entity, string tableName = "Roles", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);

            entity.Property(r => r.Name).HasMaxLength(50).IsRequired();
            entity.Property(r => r.Description).HasMaxLength(250);

            entity.HasMany(r => r.UserRoles).WithOne(ur => ur.Role).HasForeignKey(ur => ur.RoleId).OnDelete(DeleteBehavior.Restrict);
        }

        public static void Map(EntityTypeBuilder<RoleSubRole> entity, string tableName = "RoleSubRoles", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => new { e.RoleId, e.SubRoleId });

            entity.HasOne(rsr => rsr.Role).WithMany().HasForeignKey(rsr => rsr.RoleId).OnDelete(DeleteBehavior.Restrict);
            entity.HasOne(rsr => rsr.SubRole).WithMany().HasForeignKey(rsr => rsr.SubRoleId).OnDelete(DeleteBehavior.Restrict);
        }

        public static void Map(EntityTypeBuilder<State> entity, string tableName = "States", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);

            entity.Property(s => s.Abbreviation).HasMaxLength(5).IsRequired();
            entity.Property(s => s.Name).HasMaxLength(75).IsRequired();

            entity.HasData(new State[]
            {
                new State { Id = 1, CountryId = 1, Abbreviation = "AL", Name = "Alabama" },
                new State { Id = 2, CountryId = 1, Abbreviation = "AK", Name = "Alaska" },
                new State { Id = 3, CountryId = 1, Abbreviation = "AZ", Name = "Arizona" },
                new State { Id = 4, CountryId = 1, Abbreviation = "AR", Name = "Arkansas" },
                new State { Id = 5, CountryId = 1, Abbreviation = "CA", Name = "California" },
                new State { Id = 6, CountryId = 1, Abbreviation = "CO", Name = "Colorado" },
                new State { Id = 7, CountryId = 1, Abbreviation = "CT", Name = "Connecticut" },
                new State { Id = 8, CountryId = 1, Abbreviation = "DE", Name = "Delaware" },
                new State { Id = 9, CountryId = 1, Abbreviation = "DC", Name = "District of Columbia" },
                new State { Id = 10, CountryId = 1, Abbreviation = "FL", Name = "Florida" },
                new State { Id = 11, CountryId = 1, Abbreviation = "GA", Name = "Georgia" },
                new State { Id = 12, CountryId = 1, Abbreviation = "HI", Name = "Hawaii" },
                new State { Id = 13, CountryId = 1, Abbreviation = "ID", Name = "Idaho" },
                new State { Id = 14, CountryId = 1, Abbreviation = "IL", Name = "Illinois" },
                new State { Id = 15, CountryId = 1, Abbreviation = "IN", Name = "Indiana" },
                new State { Id = 16, CountryId = 1, Abbreviation = "IA", Name = "Iowa" },
                new State { Id = 17, CountryId = 1, Abbreviation = "KS", Name = "Kansas" },
                new State { Id = 18, CountryId = 1, Abbreviation = "KY", Name = "Kentucky" },
                new State { Id = 19, CountryId = 1, Abbreviation = "LA", Name = "Louisiana" },
                new State { Id = 20, CountryId = 1, Abbreviation = "ME", Name = "Maine" },
                new State { Id = 21, CountryId = 1, Abbreviation = "MD", Name = "Maryland" },
                new State { Id = 22, CountryId = 1, Abbreviation = "MA", Name = "Massachusetts" },
                new State { Id = 23, CountryId = 1, Abbreviation = "MI", Name = "Michigan" },
                new State { Id = 24, CountryId = 1, Abbreviation = "MN", Name = "Minnesota" },
                new State { Id = 25, CountryId = 1, Abbreviation = "MS", Name = "Mississippi" },
                new State { Id = 26, CountryId = 1, Abbreviation = "MO", Name = "Missouri" },
                new State { Id = 27, CountryId = 1, Abbreviation = "MT", Name = "Montana" },
                new State { Id = 28, CountryId = 1, Abbreviation = "NE", Name = "Nebraska" },
                new State { Id = 29, CountryId = 1, Abbreviation = "NV", Name = "Nevada" },
                new State { Id = 30, CountryId = 1, Abbreviation = "NH", Name = "New Hampshire" },
                new State { Id = 31, CountryId = 1, Abbreviation = "NJ", Name = "New Jersey" },
                new State { Id = 32, CountryId = 1, Abbreviation = "NM" , Name = "New Mexico"},
                new State { Id = 33, CountryId = 1, Abbreviation = "NY", Name = "New York" },
                new State { Id = 34, CountryId = 1, Abbreviation = "NC", Name = "North Carolina" },
                new State { Id = 35, CountryId = 1, Abbreviation = "ND", Name = "North Dakota" },
                new State { Id = 36, CountryId = 1, Abbreviation = "OH", Name = "Ohio" },
                new State { Id = 37, CountryId = 1, Abbreviation = "OK", Name = "Oklahoma" },
                new State { Id = 38, CountryId = 1, Abbreviation = "OR", Name = "Oregon" },
                new State { Id = 39, CountryId = 1, Abbreviation = "PA", Name = "Pennsylvania" },
                new State { Id = 40, CountryId = 1, Abbreviation = "PR", Name = "Puerto Rico" },
                new State { Id = 41, CountryId = 1, Abbreviation = "RI", Name = "Rhode Island" },
                new State { Id = 42, CountryId = 1, Abbreviation = "SC", Name = "South Carolina" },
                new State { Id = 43, CountryId = 1, Abbreviation = "SD", Name = "South Dakota" },
                new State { Id = 44, CountryId = 1, Abbreviation = "TN", Name = "Tennessee" },
                new State { Id = 45, CountryId = 1, Abbreviation = "TX", Name = "Texas" },
                new State { Id = 46, CountryId = 1, Abbreviation = "UT", Name = "Utah" },
                new State { Id = 47, CountryId = 1, Abbreviation = "MT", Name = "Vermont" },
                new State { Id = 48, CountryId = 1, Abbreviation = "VA", Name = "Virginia" },
                new State { Id = 49, CountryId = 1, Abbreviation = "WA", Name = "Washington" },
                new State { Id = 50, CountryId = 1, Abbreviation = "WV", Name = "West Virginia" },
                new State { Id = 51, CountryId = 1, Abbreviation = "WI", Name = "Wisconsin" },
                new State { Id = 52, CountryId = 1, Abbreviation = "WY", Name = "Wyoming" },
            });
        }

        public static void Map(EntityTypeBuilder<User> entity, string tableName = "Users", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => e.Id);

            entity.Property(u => u.Username).HasMaxLength(50).IsRequired();
            entity.Property(u => u.Password).HasMaxLength(1024).IsRequired();

            entity.HasOne(u => u.CustomerProfile).WithOne(cp => cp.User).HasForeignKey<CustomerProfile>(cp => cp.UserId).OnDelete(DeleteBehavior.Restrict);
            entity.HasMany(u => u.EmailAddresses).WithOne(ea => ea.User).HasForeignKey(ea => ea.UserId).OnDelete(DeleteBehavior.Restrict);
            entity.HasMany(u => u.Passwords).WithOne(p => p.User).HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.Restrict);
            entity.HasMany(u => u.UserRoles).WithOne(ur => ur.User).HasForeignKey(ur => ur.UserId).OnDelete(DeleteBehavior.Restrict);
        }

        public static void Map(EntityTypeBuilder<UserRole> entity, string tableName = "UserRoles", string schemaName = "dbo")
        {
            entity.ToTable(tableName, schemaName);
            entity.HasKey(e => new { e.UserId, e.RoleId });
        }

        //public static void Map(EntityTypeBuilder<TEntity> entity, string tableName = "Entities", string schemaName = "dbo")
        //{
        //    entity.ToTable(tableName, schemaName);
        //    entity.HasKey(e => e.Id);
        //}
    }
}
