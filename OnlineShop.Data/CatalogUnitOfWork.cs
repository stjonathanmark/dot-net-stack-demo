﻿using System;
using System.Collections.Generic;
using System.Text;
using OnlineShop.Data.Repositories;

namespace OnlineShop.Data
{
    public class CatalogUnitOfWork : UnitOfWork, ICatalogUnitOfWork
    {
        public CatalogUnitOfWork(DataSource context)
            : base(context)
        {
            Categories = new CategoryRepository(context);
            Products = new ProductRepository(context);
        }

        public ICategoryRepository Categories { get; protected set; }

        public IProductRepository Products { get; protected set; }

        public override void Commit()
        {
            base.Commit();
            AddRepoInfoToUnitOfWorkInfo(Categories.RepositoryInfo);
            AddRepoInfoToUnitOfWorkInfo(Products.RepositoryInfo);

        }

        private void AddRepoInfoToUnitOfWorkInfo(Dictionary<string, object> info)
        {
            if (info.Count > 0)
            {
                foreach(KeyValuePair<string, object> kvp in info)
                {
                    UnitOfWorkInfo.Add(kvp.Key, kvp.Value);
                }
            }
        }

        public int? CategoriesFound
        {
           get
            {
                int? itemsFound = null;

                string key = Categories.BaseKey + "ItemsFound";

                if (Categories.RepositoryInfo.ContainsKey(key)) {
                    itemsFound = (int)Categories.RepositoryInfo[key];
                }

                return itemsFound;
            }
        }

        public int? ProductsFound
        {
            get
            {
                int? itemsFound = null;

                string key = Products.BaseKey + "ItemsFound";

                if (Products.RepositoryInfo.ContainsKey(key))
                {
                    itemsFound = (int)Products.RepositoryInfo[key];
                }

                return itemsFound;
            }
        }
    }
}
