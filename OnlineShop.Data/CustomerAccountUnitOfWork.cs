﻿using OnlineShop.Data.Repositories;

namespace OnlineShop.Data
{
    public class CustomerAccountUnitOfWork : UnitOfWork, ICustomerAccountUnitOfWork
    {

        public CustomerAccountUnitOfWork(DataSource context)
            : base(context)
        {
            Users = new UserRepository(context);
        }

        public IUserRepository Users { get; set; }
    }
}
