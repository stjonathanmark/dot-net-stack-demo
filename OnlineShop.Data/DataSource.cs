﻿using Microsoft.EntityFrameworkCore;
using OnlineShop.Model;

namespace OnlineShop.Data
{
    public class DataSource : DbContext
    {
        string connString;

        public DataSource()
        {
            // Modify Connection String To Use Desired Database
            connString = "Data Source=DbServerNameOrAddress;Initial Catalog=DatabaseName;User Id=DbUsername;Password=DbPassword";
        }

        public DataSource(string connectionString)
        {
            connString = connectionString;
        }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<CustomerProfile> CustomerProfiles { get; set; }

        public DbSet<EmailAddress> EmailAddresses { get; set; }

        public DbSet<Option> Options { get; set; }

        public DbSet<OptionSet> OptionSets { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderedItem> OrderedItems { get; set; }

        public DbSet<Password> Passwords { get; set; }

        public DbSet<PaymentMethod> PaymentMethods { get; set; }

        public DbSet<PhoneNumber> PhoneNumbers { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<State> States { get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            DataMapper.Map(modelBuilder.Entity<User>());
            DataMapper.Map(modelBuilder.Entity<Address>());

            DataMapper.Map(modelBuilder.Entity<Category>());
            DataMapper.Map(modelBuilder.Entity<Country>());
            DataMapper.Map(modelBuilder.Entity<CustomerProfile>());

            DataMapper.Map(modelBuilder.Entity<EmailAddress>());

            DataMapper.Map(modelBuilder.Entity<Password>());
            DataMapper.Map(modelBuilder.Entity<PaymentMethod>());
            DataMapper.Map(modelBuilder.Entity<PhoneNumber>());
            DataMapper.Map(modelBuilder.Entity<Product>());

            DataMapper.Map(modelBuilder.Entity<Option>());
            DataMapper.Map(modelBuilder.Entity<OptionSet>());
            DataMapper.Map(modelBuilder.Entity<Order>());
            DataMapper.Map(modelBuilder.Entity<OrderedItem>());
            DataMapper.Map(modelBuilder.Entity<OrderedItemOption>());

            DataMapper.Map(modelBuilder.Entity<Role>());
            DataMapper.Map(modelBuilder.Entity<RoleSubRole>());

            DataMapper.Map(modelBuilder.Entity<State>());
            DataMapper.Map(modelBuilder.Entity<UserRole>());
        }
    }
}
