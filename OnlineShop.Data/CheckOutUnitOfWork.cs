﻿using Microsoft.EntityFrameworkCore;
using OnlineShop.Data.Repositories;

namespace OnlineShop.Data
{
    public class CheckOutUnitOfWork : UnitOfWork, ICheckOutUnitOfWork
    {
        public CheckOutUnitOfWork(DataSource context)
            : base(context)
        {
            CustomerProfiles = new CustomerProfileRepository(context);
            PaymentMethods = new PaymentMethodRepository(context);
            Orders = new OrderRepository(context);
        }

        public ICustomerProfileRepository CustomerProfiles { get; set; }

        public IPaymentMethodRepository PaymentMethods { get; set; }

        public IOrderRepository Orders { get; set; }
    }
}
