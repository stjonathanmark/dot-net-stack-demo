﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShop.Data
{
    public abstract class UnitOfWork : IUnitOfWork
    {
        protected readonly string baseUowKey;
        protected DataSource context;

        public UnitOfWork(DataSource context)
        {
            baseUowKey = this.GetType().Name + "-";
            this.context = context;
            UnitOfWorkInfo = new Dictionary<string, object>();
        }

        public Dictionary<string, object> UnitOfWorkInfo { get; set; }

        public virtual void Commit()
        {
            ClearUnitOfWorkInfo();
            int rowsEffected = context.SaveChanges();
            AddUnitOfWorkInfo("RowsEffected", rowsEffected);
        }

        protected void AddUnitOfWorkInfo(string key, object value)
        {
            string uowKey = baseUowKey + key;

            UnitOfWorkInfo.Add(uowKey, value);

        }

        protected void ClearUnitOfWorkInfo()
        {
            UnitOfWorkInfo.Clear();
        }
    }
}
