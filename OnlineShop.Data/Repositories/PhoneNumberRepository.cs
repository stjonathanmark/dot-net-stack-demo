﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class PhoneNumberRepository : Repository<PhoneNumber, long>, IPhoneNumberRepository
    {
        public PhoneNumberRepository(DataSource context)
            : base(context)
        {

        }
    }
}