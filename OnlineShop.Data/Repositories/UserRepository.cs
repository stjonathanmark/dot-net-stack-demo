﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class UserRepository : Repository<User, long>, IUserRepository
    {
        public UserRepository(DataSource context)
            : base(context)
        {
        }

        public User GetByUsername(string username)
        {
            User user = context.Users.FirstOrDefault(u => u.Username.ToLower() == username.ToLower());

            return user;
        }

        public User GetForTokenCreation(long userId)
        {
            User user = context.Users.Include(u => u.CustomerProfile).Include(u => u.UserRoles).ThenInclude(ur => ur.Role).FirstOrDefault(u => u.Id == userId);

            return user;
        }

        public long GetIdByUsername(string username)
        {
            long userId = context.Users.FirstOrDefault(u => u.Username.ToLower() == username.ToLower()).Id;

            return userId;
        }

        public bool UsernameExists(string username)
        {
            bool exists = context.Users.Any(u => u.Username.ToLower() == username.ToLower());

            return exists;
        }
    }
}
