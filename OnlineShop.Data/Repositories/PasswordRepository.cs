﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class PasswordRepository : Repository<Password, long>, IPasswordRepository
    {
        public PasswordRepository(DataSource context)
            : base(context)
        {

        }
    }
}
