﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class PaymentMethodRepository : Repository<PaymentMethod, long>, IPaymentMethodRepository
    {
        public PaymentMethodRepository(DataSource context)
            : base(context)
        {
        }
    }
}
