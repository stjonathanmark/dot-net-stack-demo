﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class AddressRepository : Repository<Address, long>, IAddressRepository
    {
        public AddressRepository(DataSource context)
            : base(context)
        {

        }
    }
}
