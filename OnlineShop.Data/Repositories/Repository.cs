﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace OnlineShop.Data.Repositories
{
    public abstract class Repository<TEntity, TEntityId> : IRepository<TEntity, TEntityId>
        where TEntityId : struct
        where TEntity : BaseEntity<TEntityId>
    {
        internal DataSource context;

        public Repository(DataSource context)
        {
            this.context = context;
            RepositoryInfo = new Dictionary<string, object>();
        }

        public Dictionary<string, object> RepositoryInfo { get; protected set; }

        public void Add(TEntity entity)
        {
            context.Set<TEntity>().Add(entity);
        }

        public void Add(IEnumerable<TEntity> entities)
        {
            context.Set<TEntity>().AddRange(entities);
        }

        public TEntity Get(TEntityId id)
        {
            TEntity entity = context.Set<TEntity>().Find(id);

            return entity;
        }

        public IEnumerable<TEntity> Get(IEnumerable<TEntityId> ids)
        {
            IEnumerable<TEntity> entities = context.Set<TEntity>().Where(e => ids.Contains(e.Id)).ToList();

            return entities;
        }

        public IEnumerable<TEntity> Get(int? skip = null, int? take = null)
        {
            ClearRepositoryInfo();

            int itemsFound = context.Set<TEntity>().Count();

            AddRepositoryInfo("ItemsFound", itemsFound);

            IEnumerable<TEntity> entities = new List<TEntity>();

            if (skip.HasValue && take.HasValue)
            {
                entities = context.Set<TEntity>().Skip(skip.Value).Take(take.Value).ToList();
            }
            else if (skip.HasValue)
            {
                entities = context.Set<TEntity>().Skip(skip.Value).ToList();
            }
            else if (take.HasValue)
            {
                entities = context.Set<TEntity>().ToList();
            }
            else
            {
                entities = context.Set<TEntity>().ToList();
            }
            
            return entities;
        }

        public void Remove(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
        }

        public void Remove(IEnumerable<TEntity> entities)
        {
            context.Set<TEntity>().RemoveRange(entities);
        }

        public void Update(TEntity entity)
        {
            context.Set<TEntity>().Update(entity);
        }

        public void Update(IEnumerable<TEntity> entities)
        {
            context.Set<TEntity>().UpdateRange(entities);
        }

        public IEnumerable<TEntity> Search(Func<TEntity, bool> predicate, int? skip = null, int? take = null)
        {
            ClearRepositoryInfo();

            int itemsFound = context.Set<TEntity>().Count(predicate);

            AddRepositoryInfo("ItemsFound", itemsFound);

            IEnumerable<TEntity> entities = new List<TEntity>();

            if (skip.HasValue && take.HasValue)
            {
                entities = context.Set<TEntity>().Where(predicate).Skip(skip.Value).Take(take.Value).ToList();
            }
            else if (skip.HasValue)
            {
                entities = context.Set<TEntity>().Where(predicate).Skip(skip.Value).ToList();
            }
            else if (take.HasValue)
            {
                entities = context.Set<TEntity>().Where(predicate).Take(take.Value).ToList();
            }
            else
            {
                entities = context.Set<TEntity>().Where(predicate).ToList();
            }
            
            return entities;
        }

        protected void AddRepositoryInfo(string key, object value)
        {
            string repoKey = BaseKey + key;

            RepositoryInfo.Add(repoKey, value);
        }

        protected void ClearRepositoryInfo()
        {
            RepositoryInfo.Clear();
        }

        public string BaseKey
        {
            get
            {
                string repoTypeName = typeof(TEntity).Name;

                return repoTypeName + "-";
            }
        }
    }
}
