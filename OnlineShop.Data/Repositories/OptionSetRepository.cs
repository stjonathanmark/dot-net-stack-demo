﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class OptionSetRepository : Repository<OptionSet, int>, IOptionSetRepository
    {
        public OptionSetRepository(DataSource context)
            : base(context)
        {
        }
    }
}
