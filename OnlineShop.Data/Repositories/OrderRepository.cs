﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class OrderRepository : Repository<Order, long>, IOrderRepository
    {
        public OrderRepository(DataSource context)
            : base(context)
        {
        }
    }
}
