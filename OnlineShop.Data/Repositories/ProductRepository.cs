﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class ProductRepository :  Repository<Product, int>, IProductRepository
    {

        public ProductRepository(DataSource context)
            : base(context)
        {
        }

        public IEnumerable<Product> GetWithCategories(int? skip = null, int? take = null)
        {
            ClearRepositoryInfo();

            int itemsFound = context.Products.Count();

            AddRepositoryInfo("ItemsFound", itemsFound);

            IEnumerable<Product> products = new List<Product>();

            if (skip.HasValue && take.HasValue)
            {
                products = context.Products.Include(p => p.Category).Skip(skip.Value).Take(take.Value).ToList();
            }
            else if (skip.HasValue)
            {
                products = context.Products.Include(p => p.Category).Skip(skip.Value).ToList();
            }
            else if (take.HasValue)
            {
                products = context.Products.Include(p => p.Category).ToList();
            }
            else
            {
                products = context.Products.Include(p => p.Category).ToList();
            }

            return products;
        }

        public IEnumerable<Product> GetWithCategoriesByCategory(int categoryId, int? skip = null, int? take = null)
        {
            ClearRepositoryInfo();

            int itemsFound = context.Products.Where(p => p.CategoryId == categoryId).Count();

            AddRepositoryInfo("ItemsFound", itemsFound);

            IEnumerable<Product> products = new List<Product>();

            if (skip.HasValue && take.HasValue)
            {
                products = context.Products.Where(p => p.CategoryId == categoryId).Include(p => p.Category).Skip(skip.Value).Take(take.Value).ToList();
            }
            else if (skip.HasValue)
            {
                products = context.Products.Where(p => p.CategoryId == categoryId).Include(p => p.Category).Skip(skip.Value).ToList();
            }
            else if (take.HasValue)
            {
                products = context.Products.Where(p => p.CategoryId == categoryId).Include(p => p.Category).ToList();
            }
            else
            {
                products = context.Products.Where(p => p.CategoryId == categoryId).Include(p => p.Category).ToList();
            }

            return products;
        }

        public Product GetWithCategory(int productId)
        {
            Product product = context.Products.Include(p => p.Category).FirstOrDefault(p => p.Id == productId);

            return product;
        }

    }
}
