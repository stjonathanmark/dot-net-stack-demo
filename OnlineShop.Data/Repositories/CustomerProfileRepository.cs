﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class CustomerProfileRepository : Repository<CustomerProfile, long>, ICustomerProfileRepository
    {
        public CustomerProfileRepository(DataSource context)
            : base(context)
        {

        }

        public CustomerProfile GetWithContactInformation(long customerProfileId)
        {
            CustomerProfile profile = context.CustomerProfiles.Include(cp => cp.EmailAddresses).Include(cp => cp.PhoneNumbers).Include(cp => cp.Addresses).FirstOrDefault(cp => cp.Id == customerProfileId);

            return profile;
        }
    }
}
