﻿using OnlineShop.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace OnlineShop.Data.Repositories
{
    public class OrderedItemRepository : Repository<OrderedItem, long>, IOrderedItemRepository
    {
        public OrderedItemRepository(DataSource context)
            : base(context)
        {
        }
        
        public IEnumerable<OrderedItem> GetOrderedItemsWithRelevantInfoForOrder(long orderId)
        {

            IEnumerable<OrderedItem> orderedItems = context.OrderedItems
                                                                    .Include(oi => oi.Product)
                                                                        .ThenInclude(p => p.Category)
                                                         .Select(oi => new OrderedItem() {
                                                             OrderId = oi.OrderId,
                                                             Price = oi.Price,
                                                             Product = new Product() {
                                                                 Name = oi.Product.Name,
                                                                 Price = oi.Product.Price,
                                                                 Category = new Category()  {
                                                                     Name = oi.Product.Category.Name
                                                                 }
                                                             }
                                                         }).ToList();

            return orderedItems;
        }
    }
}

