﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using OnlineShop.Model;


namespace OnlineShop.Data.Repositories
{
    public class CategoryRepository : Repository<Category, int>, ICategoryRepository
    {
        public CategoryRepository(DataSource context)
            : base(context)
        {
        }

        public IEnumerable<Category> GetByCategory(int categoryId)
        {
            IEnumerable<Category> categories = context.Categories.Where(c => c.ParentCategoryId == categoryId).ToList();

            return categories;
        }
    }
}
