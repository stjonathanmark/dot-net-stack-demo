﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class EmailAddressRepository : Repository<EmailAddress, long>, IEmailAddressRepository
    {
        public EmailAddressRepository(DataSource context)
            : base(context)
        {

        }
    }
}
