﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class StateRepository : Repository<State, int>, IStateRepository
    {
        public StateRepository(DataSource context)
            : base(context)
        {

        }
    }
}