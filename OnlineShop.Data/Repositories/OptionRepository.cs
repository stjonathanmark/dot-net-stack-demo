﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class OptionRepository : Repository<Option, int>, IOptionRepository
    {
        public OptionRepository(DataSource context)
            : base(context)
        {
        }
    }
}
