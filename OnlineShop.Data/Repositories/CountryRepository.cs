﻿using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class CountryRepository : Repository<Country, int>, ICountryRepository
    {
        public CountryRepository(DataSource context)
            : base(context)
        {

        }
    }
}