﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using OnlineShop.Model;

namespace OnlineShop.Data.Repositories
{
    public class RoleRepository : Repository<Role, int>, IRoleRepository
    {
        public RoleRepository(DataSource context)
            : base(context)
        {
            
        }

        public Role GetByName(string name)
        {
            Role role = context.Roles.FirstOrDefault(r => r.Name.ToLower() == name.ToLower());

            return role;
        }

        public IEnumerable<Role> GetByNames(IEnumerable<string> names)
        {
            IEnumerable<Role> roles = context.Roles.Where(r => names.Select(n => n.ToLower()).Contains(r.Name.ToLower())).ToList();

            return roles;
        }
    }
}
