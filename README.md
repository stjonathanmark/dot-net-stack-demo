# Online Shop Demo
This is a demo e-commerce web app that was created with ASP.Net Core 3.1, EntityFramework Core 3.1.7, and Angular 8.

## Instructions to run the application in your local environment using Visual Studio 2019
1. Create a test account on [Stripe.com](https://stripe.com) and replace the parameter string in the **"StripeConfiguration.SetApiKey()"** method with the test secret key for the account on **"line 27"** in the [StripeService.cs code file](https://bitbucket.org/stjonathanmark/dot-net-stack-demo/src/master/OnlineShop.Model/Payments/Stripe/StripeService.cs) in **"/payments/stripe/"** folder the **"OnlineShop.Models"** project.
2. Replace the string value being assigned to the **"connString"** field on **"line 13"** in the [DataSource.cs code file](https://bitbucket.org/stjonathanmark/dot-net-stack-demo/src/master/OnlineShop.Data/DataSource.cs) in the root of the **"OnlineShop.Data"** project with the connection string for the test database server and the name of the database to be created using migration in next step. 
3. Make **"OnlineShop.Data"** the start up project, and go to the **"Package Manager Console"** and type and run the command **"Update-Database"** to create the database.
4. Right click on the **"ClientApp"** folder at root of **"OnlineShop.Web"** project, hover over the **"Open Command Line"** option in the context menu, then choose the command line tool of your preference
5. In the command line tool, run the following commands:
    1. npm i
    2. npm audit fix
    3. npm i @angular-devkit/build-angular@0.803.26 
        * **NOTE**: Due to issues with the VS 2019 Angular Template, package **"@angular-devkit/build-angular"** must be at **version 0.803.26** (Not the latest) in order for the angular app to run in VS 2019 (At least at the time of uploading this code).
        * It is likely you will see vunerabilities after this command complete, but not many. Do not run **"npm audit fix"** or it will upgrade the **"@angular-devkit/build-angular"** package to it's latest version.
        * If you wish to view the vunerabilities run the **"npm audit"** command. This will allow you to address each vunerabity individually without affecting the verson of the **"@angular-devkit/build-angular"** package
        * Due to the fact that this is a demo, I would reccomend not worrying about fixing any vunerabilities that appear after running the 3rd command, unless one of those vunerabilites prevent you from running the app.
6. Finally make **"OnlineShop.Web"** the start up project and press **"F5"** to start the application.
7. Enjoy!!!!!!!