﻿using System.Collections.Generic;
using OnlineShop.Service;
using OnlineShop.Dto;

namespace OnlineShop.Web.Models
{
    public class ProductResponse : ViewResponse
    {
        public ProductResponse()
            : base()
        {
        }

        public ProductResponse(Response response)
            : base (response)
        {
        }

        public ProductResponse(CatalogResponse response)
            : base(response)
        {
            Products = response.Products;
            Product = response.Product;

        }

        public IEnumerable<ProductDto> Products { get; set; }

        public ProductDto Product { get; set; }
    }
}
