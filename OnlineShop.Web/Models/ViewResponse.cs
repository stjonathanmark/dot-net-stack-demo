﻿using System.Collections.Generic;
using OnlineShop.Service;

namespace OnlineShop.Web.Models
{
    public class ViewResponse : Response
    {
        public ViewResponse()
            : base()
        {
        }

        public ViewResponse(Response response)
            : base(response)
        {
        }
    }
}
