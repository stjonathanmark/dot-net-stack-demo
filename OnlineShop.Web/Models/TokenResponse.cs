﻿using OnlineShop.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShop.Web.Models
{
    public class TokenResponse : ViewResponse
    {
        public TokenResponse()
            : base()
        {
        }

        public TokenResponse(SecurityResponse response) 
            : base(response)
        {
            AccountCreationResult = response.AccountCreationResult;
            AuthenticationResult = response.AuthenticationResult;
            Authenticated = response.Authenticated;
            AuthenticationMessage = response.AuthenticationMessage;
            AccountCreationMessage = response.AccountCreationMessage;
            AccountCreated = response.AccountCreated;

        }

        public AccountCreationResult AccountCreationResult { get; set; }

        public AuthenticationResult AuthenticationResult { get; set; }

        public bool Authenticated { get; set; }

        public string AuthenticationMessage { get; set; }

        public string AccountCreationMessage { get; set; }

        public bool AccountCreated { get; set; }

        public string Token { get; set; }
    }
}
