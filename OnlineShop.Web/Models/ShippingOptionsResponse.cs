﻿using System.Collections.Generic;
using OnlineShop.Dto;
using OnlineShop.Service;

namespace OnlineShop.Web.Models
{
    public class ShippingOptionsResponse : ViewResponse
    {
        public ShippingOptionsResponse()
        {

        }

        public ShippingOptionsResponse(CheckOutResponse response)
            : base(response)
        {
            States = response.States;
        }

        public IEnumerable<StateDto> States { get; set; }
    }
}
