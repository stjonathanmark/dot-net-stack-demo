﻿using System.Collections.Generic;
using OnlineShop.Dto;

namespace OnlineShop.Web.Models
{
    public class PaymentRequest
    {
        public long CustomerProfileId { get; set; }

        public long? PaymentMethodId { get; set; }

        public string PaymentTokenId { get; set; }

        public string CreationTokenId { get; set; }

        public IEnumerable<ProductDto> Products { get; set; }

        public AddressDto ShippingAddress { get; set; }

        public CardDto Card { get; set; }

        public decimal SubTotal { get; set; }

        public decimal ShippingCost { get; set; }

        public decimal Tax { get; set; }

        public decimal Total { get; set; }
    }
}
