﻿using OnlineShop.Service;
using OnlineShop.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShop.Web.Models
{
    public class PaymentResponse : ViewResponse
    {
        public PaymentResponse()
            : base()
        {

        }

        public PaymentResponse(CheckOutResponse response)
            : base(response)
        {
            PaymentApproved = response.PaymentApproved;
            PaymentMessage = response.PaymentMessage;
            PaymentToken = response.PaymentToken;
        }

        public bool PaymentApproved { get; set; }

        public string PaymentMessage { get; set; }

        public PaymentTokenDto PaymentToken { get; set; }
    }
}
