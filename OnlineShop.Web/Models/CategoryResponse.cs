﻿using System.Collections.Generic;
using OnlineShop.Service;
using OnlineShop.Dto;

namespace OnlineShop.Web.Models
{
    public class CategoryResponse : ViewResponse
    {
        public CategoryResponse()
            : base()
        {
        }

        public CategoryResponse(Response response)
            : base (response)
        {
        }

        public CategoryResponse(CatalogResponse response)
            : base(response)
        {
            Categories = response.Categories;
            Category = response.Category;


        }

        public IEnumerable<CategoryDto> Categories { get; set; }

        public CategoryDto Category { get; set; }

        public int? CategoryId
        {

            get
            {
                int? id = null;

                if (Category != null)
                {
                    id = Category.Id;
                }

                return id;
            }
        }
    }
}
