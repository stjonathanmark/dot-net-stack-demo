﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using OnlineShop.Service;
using Microsoft.Extensions.Configuration;
using OnlineShop.Dto;

namespace OnlineShop.Web.Models
{
    public class ViewService
    {
        OnlineShopService service = new OnlineShopService();

        IConfiguration config;

        public ViewService(IConfiguration configuration)
        {
            config = configuration;
        }

        #region Security

        public TokenResponse RegisterUserAccount(UserDto user)
        {
            var request = new SecurityRequest(SecurityRequestType.CreateUserAccount)
            {
                User = user,
                AuthenticatedUserOnCreation = true
            };

            request.RoleNamesToAdd.Add("Customer");

            var response = service.MakeRequest(request);

            var tokenResponse = new TokenResponse(response);

            if (response.Successful && response.Authenticated)
            {
                tokenResponse.Token = GenerateSecurityToken(response.TokenUser);
            }

            return tokenResponse;
        }

        public TokenResponse AuthenticateUser(string username, string password)
        {
            var request = new SecurityRequest(SecurityRequestType.AuthenticateUser)
            {
                Username = username,
                Password = password
            };

            var response = service.MakeRequest(request);

            var tokenResponse = new TokenResponse(response);

            if (response.Successful && response.Authenticated)
            {
                tokenResponse.Token = GenerateSecurityToken(response.TokenUser);
            }

            return tokenResponse;
        }

        #endregion  
        
        #region Catalog

        public CategoryResponse GetAllCategories()
        {
            CatalogRequest request = new CatalogRequest();
            request.Type = CatalogRequestType.GetAllCategories;

            CatalogResponse response = service.MakeRequest(request);

            CategoryResponse categoryResponse = new CategoryResponse(response);

            return categoryResponse;
        }

        public CategoryResponse GetCategoryByProduct(int productId)
        {
            CatalogRequest request = new CatalogRequest();
            request.Type = CatalogRequestType.GetCategoryByProduct;
            request.ProductId = productId;

            CatalogResponse response = service.MakeRequest(request);

            CategoryResponse categoryResponse = new CategoryResponse(response);

            return categoryResponse;
        }

        public CategoryResponse GetCategory(int categoryId)
        {
            CatalogRequest request = new CatalogRequest();
            request.Type = CatalogRequestType.GetCategory;
            request.CategoryId = categoryId;

            CatalogResponse response = service.MakeRequest(request);

            CategoryResponse categoryResponse = new CategoryResponse(response);

            return categoryResponse;
        }

        public ProductResponse GetAllProducts(int? pageNumber, int? pageSize, int? medianPage)
        {
            CatalogRequest request = new CatalogRequest();
            request.Type = CatalogRequestType.GetAllProducts;

            SetPaging(request, pageNumber, pageSize, medianPage);

            CatalogResponse response = service.MakeRequest(request);

            ProductResponse productResponse = new ProductResponse(response);

            return productResponse;
        }

        public ProductResponse GetProductByCategory(int categoryId, int? pageNumber, int? pageSize, int? medianPage)
        {
            CatalogRequest request = new CatalogRequest();
            request.Type = CatalogRequestType.GetProductsByCategory;
            request.CategoryId = categoryId;

            SetPaging(request, pageNumber, pageSize, medianPage);

            CatalogResponse response = service.MakeRequest(request);

            ProductResponse productResponse = new ProductResponse(response);

            return productResponse;
        }

        public ProductResponse GetProductDetails(int productId)
        {
            CatalogRequest request = new CatalogRequest();
            request.Type = CatalogRequestType.GetProductDetails;
            request.ProductId = productId;

            CatalogResponse response = service.MakeRequest(request);

            ProductResponse productResponse = new ProductResponse(response);

            return productResponse;
        }

        private void SetPaging(Request request, int? pageNumber, int? pageSize, int? medianPage)
        {
            if (pageNumber.HasValue && pageSize.HasValue)
            {
                request.PageItems = true;
                request.PageNumber = pageNumber.Value;
                request.PageSize = pageSize.Value;
                request.MedianPage = medianPage.Value;
            }
        }

        #endregion

        #region Check Out 

        public ShippingOptionsResponse GetShippingOptions()
        {
            CheckOutRequest request = new CheckOutRequest(CheckOutRequestType.GetShippingOptions);

            CheckOutResponse response = service.MakeRequest(request);

            ShippingOptionsResponse shippingOptsResponse = new ShippingOptionsResponse(response);

            return shippingOptsResponse;
        }

        public PaymentResponse MakePayment(long customerProfileId, long? paymentMethodId, string paymentTokenId, string creationTokenId, IEnumerable<ProductDto> products, AddressDto shippingAddress, decimal subTotal, decimal shippingCost, decimal tax, decimal total)
        {
            CheckOutRequest request = new CheckOutRequest(CheckOutRequestType.MakePayment) {
                CustomerProfileId = customerProfileId,
                PaymentMethodId = paymentMethodId,
                PaymentTokenId = paymentTokenId,
                CreationTokenId = creationTokenId,
                Products = products,
                ShippingAddress = shippingAddress,
                SubTotal = subTotal,
                ShippingCost = shippingCost,
                Tax = tax,
                Total = total
            };
            
            CheckOutResponse response = service.MakeRequest(request);

            PaymentResponse paymentResponse = new PaymentResponse(response);

            return paymentResponse;
        }

        public PaymentResponse GetPaymentToken(CardDto card)
        {
            var request = new CheckOutRequest(CheckOutRequestType.GetPaymentToken)
            {
                Card = card
            };

            var response = service.MakeRequest(request);

            var paymentResponse = new PaymentResponse(response);

            return paymentResponse;
        }

        #endregion

        private string GenerateSecurityToken(TokenUserDto tokenUser)
        {
            //StringBuilder roles = new StringBuilder();
            
            var claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Sub, tokenUser.Username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("uid", tokenUser.UserId.ToString()),
            };

            if (tokenUser.Roles != null && tokenUser.Roles.Any())
            {
                foreach (var role in tokenUser.Roles)
                {
                    claims.Add(new Claim("rls", role.Name));
                }
            }
            
            if (tokenUser.CustomerId.HasValue)
            {
                claims.Add(new Claim("cid", tokenUser.CustomerId.Value.ToString()));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["Token:SigningKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(config["Token:Issuer"], config["Token:Audience"], claims, DateTime.Now, DateTime.Now.AddHours(1), creds);

            var handler = new JwtSecurityTokenHandler();
            var generatedToken = handler.WriteToken(token);

            return generatedToken;
        }
    }
}
