﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OnlineShop.Web.Models;

namespace OnlineShop.Web.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : ControllerBase
    {
        ViewService service;

        public AccountController(IConfiguration configuration)
        {
            service = new ViewService(configuration);
        }
    }
}