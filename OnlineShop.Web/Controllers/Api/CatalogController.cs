﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineShop.Web.Models;
using Microsoft.Extensions.Configuration;

namespace OnlineShop.Web.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Catalog")]
    public class CatalogController : ControllerBase
    {
        ViewService service;

        public CatalogController(IConfiguration configuration)
        {
            service = new ViewService(configuration);
        }

        [Route("categories")]
        public IActionResult GetCategories()
        {
            CategoryResponse response = service.GetAllCategories();

            return Ok(response);
        }

        [Route("categories/{id}")]
        public IActionResult GetCategory(int id)
        {
            CategoryResponse response = service.GetCategory(id);

            return Ok(response);
        }

        [Route("products/{productId}/category")]
        public IActionResult GetCategoryByProduct(int productId)
        {
            CategoryResponse response = service.GetCategoryByProduct(productId);

            return Ok(response);
        }

        [Route("products")]
        public IActionResult GetProducts(int? pageNumber, int? pageSize, int? medianPage)
        {
            ProductResponse response = service.GetAllProducts(pageNumber, pageSize, medianPage);

            return Ok(response);
        }

        [Route("products/{id}")]
        public IActionResult GetProduct(int id)
        {
            ProductResponse response = service.GetProductDetails(id);

            return Ok(response);
        }

        [Route("categories/{categoryId}/products")]
        public IActionResult GetProductsByCategory(int categoryId, int? pageNumber, int? pageSize, int? medianPage)
        {
            ProductResponse response = service.GetProductByCategory(categoryId, pageNumber, pageSize, medianPage);

            return Ok(response);
        }
    }
}