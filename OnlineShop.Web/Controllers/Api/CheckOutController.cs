﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OnlineShop.Web.Models;
using OnlineShop.Dto;
using Microsoft.AspNetCore.Authorization;

namespace OnlineShop.Web.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/CheckOut")]
    public class CheckOutController : ControllerBase
    {
        ViewService service;

        public CheckOutController(IConfiguration configuration)
        {
            service = new ViewService(configuration);
        }

        [HttpGet()]
        [Route("shipping/options")]
        public IActionResult GetShippingOptions()
        {
            ShippingOptionsResponse response = service.GetShippingOptions();

            return Ok(response);
        }

        [HttpGet]
        [Route("payment/options")]
        public IActionResult GetPaymentOptions()
        {
            return Ok();
        }

        [HttpPost]
        [Route("payment/token")]
        public IActionResult GetPaymentToken([FromBody]CardDto card)
        {
            PaymentResponse response = service.GetPaymentToken(card);

            return Ok(response);
        }

        [HttpPost]
        [Route("payment/make")]
        public IActionResult MakePayment([FromBody]PaymentRequest request)
        {
            PaymentResponse response = service.MakePayment(request.CustomerProfileId, request.PaymentMethodId, request.PaymentTokenId, request.CreationTokenId, request.Products, request.ShippingAddress, request.SubTotal, request.ShippingCost, request.Tax, request.Total);

            return Ok(response);
        } 
    }
}