﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OnlineShop.Web.Models;
using OnlineShop.Dto;

namespace OnlineShop.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Security")]
    public class SecurityController : ControllerBase
    {
        ViewService service;

        public SecurityController(IConfiguration configuration)
        {
            service = new ViewService(configuration);
        }

        [HttpPost]
        [Route("register")]
        public IActionResult RegisterUser([FromBody]UserDto user)
        {
            TokenResponse response = service.RegisterUserAccount(user);

            return Ok(response);
        }

        [HttpPost]
        [Route("authenticate")]
        public IActionResult AuthenticateUser([FromBody]LoginCredentials credentials)
        {
            TokenResponse response = service.AuthenticateUser(credentials.Username, credentials.Password);

            return Ok(response);
        }
    }
}