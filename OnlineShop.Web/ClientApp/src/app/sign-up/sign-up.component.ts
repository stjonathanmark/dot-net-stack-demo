import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { SecurityService } from '../auth/security.service';

@Component({
    selector: 'sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

    errorMessage: string;
    returnUrl: string;

    constructor(private router: Router, private route: ActivatedRoute, private securityService: SecurityService) { }

    signUp(signUpForm: NgForm) {
        this.errorMessage = '';
        let user = signUpForm.value.user;

        let emailAddress = signUpForm.value.emailAddress;
        emailAddress.isPrimary = true;

        user.customerProfile.emailAddresses = [emailAddress]

        this.securityService.registerUser(user).subscribe(response => {
            let responseObj = (response as any);

            if (responseObj.successful) {
                if (responseObj.accountCreated) {
                    if (responseObj.authenticated) {
                        this.securityService.saveToken(responseObj.token);
                        this.router.navigate([this.returnUrl]);
                    } else {
                        this.router.navigate(['/login']);
                    }

                } else {
                    this.errorMessage = responseObj.accountCreationMessage;
                }
            } else {
                this.errorMessage = responseObj.message;
            }
        });
    }

    ngOnInit() {
        this.route.queryParamMap.subscribe(query => {
            this.returnUrl = query.get('returnUrl') || '/catalog';
        })
    }
}
