import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CheckOutService } from '../../services/check-out.service';

@Component({
    selector: 'billing',
    templateUrl: './billing.component.html',
    styleUrls: ['./billing.component.css']
})
/** billing component*/
export class BillingComponent implements OnInit
{
    states: Array<any> = [];
    billingInfo: any = {};
    errorMessage: string = '';
    /** billing ctor */
    constructor(private router: Router, private checkOutService: CheckOutService) { }

    /** Called by Angular after billing component initialized */
    ngOnInit(): void {
        this.checkOutService.getShippingOptions().subscribe(response => {
            let responseObj = (response as any);
            console.log(responseObj);
            if (responseObj.successful) {
                this.states = responseObj.states;
            }
        });
    }
    
    next(billingInfo: any) {
        this.errorMessage = '';
        this.billingInfo = billingInfo.value;

        if (this.billingInfo.paymentMethod) {
            sessionStorage.setItem('billing', JSON.stringify(billingInfo.paymentMethod));
            this.router.navigate(['check-out/review']);
        } else {
            this.checkOutService.getPaymentToken(this.billingInfo.card).subscribe(response => {
                let responseObj = (response as any);

                if (responseObj.successful) {
                    sessionStorage.setItem('billing', JSON.stringify(responseObj.paymentToken));
                    this.router.navigate(['check-out/review']);
                } else {
                    this.errorMessage = responseObj.message;
                }
            });
        }
    }
}
