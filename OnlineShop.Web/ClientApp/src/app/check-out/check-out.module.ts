import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthModule } from '../auth/auth.module';

import { CheckOutComponent } from './check-out.component';
import { BillingComponent } from './billing/billing.component';
import { ShippingComponent } from './shipping/shipping.component';
import { ReviewComponent } from './review/review.component';
import { OrderSummaryComponent } from './order-summary/order-summary.component';

import { TokenService } from '../auth/token.service';
import { SecurityService } from '../auth/security.service';
import { CheckOutService } from '../services/check-out.service';
import { SharedDataService } from '../services/shared-data.service';
import { AuthGuard } from '../auth/auth-guard.service';

@NgModule({
  imports: [
        AuthModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: 'check-out', component: CheckOutComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard], children: [
                    { path: '', component: ShippingComponent },
                    { path: 'shipping', component: ShippingComponent },
                    { path: 'billing', component: BillingComponent },
                    { path: 'review', component: ReviewComponent }
                ]
            }
        ]),
        AuthModule
    ],
    exports: [],
    declarations: [
        CheckOutComponent,
        BillingComponent,
        ShippingComponent,
        ReviewComponent,
        OrderSummaryComponent
    ],
    providers: [
        TokenService,
        SecurityService,
        CheckOutService,
        SharedDataService,
        AuthGuard
    ],
})
export class CheckOutModule { }
