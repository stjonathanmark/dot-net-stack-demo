import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CheckOutService } from '../../services/check-out.service';

@Component({
    selector: 'shipping',
    templateUrl: './shipping.component.html',
    styleUrls: ['./shipping.component.css']
})
/** shipping component*/
export class ShippingComponent implements OnInit
{
    states: Array<any> = [];
    shippingInfo: any;

    /** shipping ctor */
    constructor(private router: Router, private checkOutService: CheckOutService) { }

    /** Called by Angular after shipping component initialized */
    ngOnInit(): void {
      this.checkOutService.getShippingOptions().subscribe(response => {
        let responseObj = (response as any);

        if (responseObj.successful) {
            this.states = responseObj.states;
        }
      });
    }

    next(shippingInfo: any) {
      this.shippingInfo = shippingInfo.value;
      let state = this.shippingInfo.address.state;
      this.shippingInfo.address.stateId = state.id;
      this.shippingInfo.address.countryId = state.countryId;
      this.shippingInfo.address.stateObj = this.shippingInfo.address.state;
      this.shippingInfo.address.state = undefined;
      sessionStorage.setItem('shipping', JSON.stringify(this.shippingInfo));
      this.router.navigate(['check-out/billing']);
    }
}
