﻿import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'check-out',
    templateUrl: './check-out.component.html',
    styleUrls: ['./check-out.component.css']
})

export class CheckOutComponent implements OnInit
{
    shippingInfo: any = null;
    billingInfo: any = null;

    constructor() { }

    ngOnInit(): void { }

    onActivate(component: any) {

        switch (component.constructor.name) {
            case 'ShippingComponent':
                break;
            case 'BilingComponent':
                break;
        }

        this.shippingInfo = JSON.parse(sessionStorage.getItem('shipping') || '');
        this.billingInfo = JSON.parse(sessionStorage.getItem('billing') || '');
    }
}