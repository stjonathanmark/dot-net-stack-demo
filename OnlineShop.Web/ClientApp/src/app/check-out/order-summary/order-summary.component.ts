import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ShoppingCartService } from '../../services/shopping-cart.service';

@Component({
    selector: 'order-summary',
    templateUrl: './order-summary.component.html',
    styleUrls: ['./order-summary.component.css']
})
/** OrderSummary component*/
export class OrderSummaryComponent implements OnInit, OnChanges
{
    @Input("shipping-info") shippingInfo: any = null;
    @Input("billing-info") billingInfo: any = null;
    paymentMethod: any = null;
    items: Array<any> = [];
    subtotal: number = 0;
    tax: number = 0;
    shippingCost: number = 0;
    total: number = 0;


    /** OrderSummary ctor */
    constructor(private shoppingCartService: ShoppingCartService) { }

    /** Called by Angular after OrderSummary component initialized */
    ngOnInit(): void {
        this.items = this.shoppingCartService.getItems();
        this.subtotal = this.shoppingCartService.calculateSubTotal();
        this.tax = this.shoppingCartService.calculateTax();
        this.total = this.shoppingCartService.calculateTotal();
    }

    ngOnChanges(): void {
        if (this.billingInfo) {
            this.paymentMethod = this.billingInfo.paymentMethod;
        }
    }
}
