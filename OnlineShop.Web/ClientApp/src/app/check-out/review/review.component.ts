import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from '../../services/shopping-cart.service';
import { CheckOutService } from '../../services/check-out.service';
import { SecurityService } from '../../auth/security.service';

@Component({
    selector: 'review',
    templateUrl: './review.component.html',
    styleUrls: ['./review.component.css']
})
/** review component*/
export class ReviewComponent implements OnInit
{
    paymentMade: boolean = false;
    processingPayment: boolean = false;
    shippingInfo: any = {};
    billingInfo: any = {};
    paymentMethod: any = {};
    products: Array<any> = [];
    cost: any = { subTotal: 0, shippingCost: 0, tax: 0, total: 0 };
    customerProfileId: number = 0;

    errorMessage: string = '';
    message: string = '';

    /** review ctor */
    constructor(private shoppingCartService: ShoppingCartService, private checkOutService: CheckOutService, private securityService: SecurityService) { }

    /** Called by Angular after review component initialized */
    ngOnInit(): void {
        this.customerProfileId = this.securityService.user.cid;
        this.products = this.shoppingCartService.getItems();
        this.cost.subTotal = this.shoppingCartService.calculateSubTotal();
        this.cost.tax = this.shoppingCartService.calculateTax();
        this.cost.total = this.shoppingCartService.calculateTotal();
        this.shippingInfo = JSON.parse(sessionStorage.getItem('shipping') || '');
        this.billingInfo = JSON.parse(sessionStorage.getItem('billing') || '');
        
        this.paymentMethod = this.billingInfo.tokenId ? this.billingInfo.paymentMethod : this.billingInfo;
    }

    onMakePayment() {
        this.processingPayment = true;
        this.message = this.errorMessage = '';
        this.checkOutService.makePayment(this.customerProfileId, this.products, this.shippingInfo, this.billingInfo, this.cost).subscribe(response => {
            let responseObj = (response as any);

            if (responseObj.successful) {

                if (responseObj.paymentApproved) {
                    this.paymentMade = true;
                    this.message = responseObj.paymentMessage;
                    sessionStorage.removeItem('billing');
                    sessionStorage.removeItem('shipping');
                    this.shoppingCartService.clearItems();
                    //Go to reciept page
                } else {
                    this.errorMessage = responseObj.paymentMessage;
                }
            } else {
                console.log(responseObj);
                this.errorMessage = responseObj.message;
            }

            this.processingPayment = false;
        });
    }
}
