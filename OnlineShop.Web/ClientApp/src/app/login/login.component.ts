import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { SecurityService } from '../auth/security.service';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    errorMessage: string = '';
    returnUrl: string;

    constructor(private route: ActivatedRoute, private router: Router, private securityService: SecurityService) { }

    login(loginForm: NgForm) {
        this.errorMessage = '';

        this.securityService.login(loginForm.value).subscribe(response => {
            let responseObj = (response as any);

            if (responseObj.successful) {
                if (responseObj.authenticated) {
                    this.securityService.saveToken(responseObj.token);
                    this.router.navigate([this.returnUrl]);
                } else {
                    this.errorMessage = responseObj.authenticationMessage;
                }
            } else {
                this.errorMessage = responseObj.message;
            }
        });
    }

    ngOnInit() {
        this.route.queryParamMap.subscribe(query => {
            this.returnUrl = query.get('returnUrl') || '/catalog';
        })
    }
}
