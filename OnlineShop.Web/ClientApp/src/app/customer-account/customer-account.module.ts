import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AuthModule } from '../auth/auth.module';

import { CustomerAccountComponent } from './customer-account.component';

import { SecurityService } from '../auth/security.service';
import { TokenService } from '../auth/token.service';
import { AuthGuard } from '../auth/auth-guard.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild([
            { path: 'customer-account', component: CustomerAccountComponent, canActivate: [AuthGuard], children: [] }
        ]),
        AuthModule
    ],
    declarations: [
        CustomerAccountComponent
    ],
    providers: [
        TokenService,
        SecurityService,
        AuthGuard
    ]
})
export class CustomerAccountModule
{
}
