﻿import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'customer-account',
    templateUrl: './customer-account.component.html',
    styleUrls: ['./customer-account.component.css']
})
/** customer-account component*/
export class CustomerAccountComponent implements OnInit
{
    /** customer-account ctor */
    constructor() { }

    /** Called by Angular after customer-account component initialized */
    ngOnInit(): void { }
}