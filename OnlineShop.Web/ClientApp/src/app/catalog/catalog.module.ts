import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { CategoriesComponent } from './categories/categories.component';
import { ProductsComponent } from './products/products.component';
import { ProductDetailsComponent } from './product-details/product-details.component';

import { CatalogComponent } from './catalog.component';
import { PagerComponent } from './../pager/pager.component';

import { CatalogService } from '../services/catalog.service';
import { ShoppingCartService } from '../services/shopping-cart.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: 'catalog', component: CatalogComponent, children: [
        { path: '', component: ProductsComponent },
        { path: 'category/:id', component: ProductsComponent },
        { path: 'product/:id', component: ProductDetailsComponent }
      ]},
    ])
  ],
  declarations: [
    CategoriesComponent, 
    ProductsComponent, 
    ProductDetailsComponent, 
    CatalogComponent,
    PagerComponent
  ],
  providers: [
      CatalogService,
      ShoppingCartService
  ]
})
export class CatalogModule { }
