import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
//import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  @Input('category-data')
  public categories: Array<any>;

  @Output('category-changed')
  public categoryChanged: EventEmitter<number> = new EventEmitter<number>();

  @Input('category-selected')
  public categorySelected: boolean = false;

  constructor(/*private route: ActivatedRoute*/) { }

  ngOnInit() {
  }

  onCategoryLinkClick(id: number) {
    this.categorySelected = Boolean(id);
    this.categoryChanged.emit(id);
  }

  

}
