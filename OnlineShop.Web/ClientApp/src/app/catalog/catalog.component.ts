import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CatalogService } from '../services/catalog.service';

@Component({
  selector: 'catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {

  categories: Array<any> = new Array<any>();
  categorySelected: boolean = false;
  private activatedCategoryId = 0;
  private initiated = false;

  constructor(private catalogService: CatalogService) { }


  ngOnInit() {
      this.catalogService.getCategories().subscribe(response => {
          this.categories = (response as any).categories;
          this.categories.forEach(category => {
              category.current = false;
          });
          this.markCurrentCategory(this.activatedCategoryId);
          this.activatedCategoryId = 0;
          this.initiated = true;
      });
  }

  onActivated(component: any) {
    component.route.paramMap.subscribe((map: ParamMap) => {
      if (map.has('id')) {
        switch(component.route.component.name)
        {
            case 'ProductsComponent':
                this.activatedCategoryId = Number.parseInt(map.get('id') || '0');
                this.markCurrentCategoryAfterInit();
            break;
          case 'ProductDetailsComponent':
                let productId: number = Number.parseInt(map.get('id') || '0');
                this.catalogService.getCategoryIdByProduct(productId).subscribe(response => {
                    this.activatedCategoryId = (response as any).categoryId;
                    this.markCurrentCategoryAfterInit();
                });
            break;
        }
      } else {
          this.activatedCategoryId = 0;
          this.markCurrentCategoryAfterInit()
      }

      
    });
  }

  markCurrentCategoryAfterInit() {
      if (this.initiated) {
          this.markCurrentCategory(this.activatedCategoryId);
      }
  }

  markCurrentCategory(categoryId: number): void {
    this.categorySelected = Boolean(categoryId);
    this.categories.forEach(category => {
      if (category.id == categoryId) {
        if (category.current) return;
        category.current = true;
      } else {
        category.current = false;
      }
    });
  }
}
