import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { CatalogService } from '../../services/catalog.service';

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  category: any = null;
  products: Array<any> = [];
  pageNumber: number = 1;
  pageSize: number = 8;
  medianPage: number = 3;
  pagingInfo: any = null;

  constructor(private route: ActivatedRoute, private catalogService: CatalogService) { }

  ngOnInit() {
    this.route.paramMap
        .subscribe(map => { 
            this.route.queryParamMap.subscribe(query => {
                let pageNum = query.get('pageNumber') || this.pageNumber;

                if (map.has('id')) {
                    let categoryId: number = Number.parseInt(map.get('id') || '0');
                    this.catalogService.getCategory(categoryId).subscribe(response => {
                        this.category = (response as any).category;
                    });
                    this.catalogService.getProductsByCategory(categoryId, pageNum, this.pageSize, this.medianPage).subscribe(response => {
                      this.products = (response as any).products
                      this.pagingInfo = (response as any).pagingInfo;
                    });
                } else {
                    this.catalogService.getProducts(pageNum, this.pageSize, this.medianPage).subscribe(response => {
                      this.products = (response as any).products;
                      this.pagingInfo = (response as any).pagingInfo;
                    });
                }
            });
    });
  }

}
