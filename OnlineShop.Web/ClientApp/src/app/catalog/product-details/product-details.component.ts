import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CatalogService } from '../../services/catalog.service';
import { ShoppingCartService } from '../../services/shopping-cart.service';

@Component({
  selector: 'product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  
    quantity: number = 1;
    product: any;

    constructor(private route: ActivatedRoute, private router: Router, private catalogService: CatalogService, private shoppingCartService: ShoppingCartService) { }

    ngOnInit() {
        this.route.paramMap.subscribe(map => {
            let productId: number = Number.parseInt(map.get('id') || '0');
            this.catalogService.getProduct(productId).subscribe(response => {
              this.product = (response as any).product;
            });
        });
    }

    onQuantityChange() {
        if (!this.quantity || this.quantity == 0) this.quantity = 1;
    }

    onAddToCart() {
        this.shoppingCartService.addItem(this.product, this.quantity);
        this.router.navigate(['/shopping-cart']);
    }

}
