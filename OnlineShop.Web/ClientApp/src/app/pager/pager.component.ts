﻿import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';

@Component({
    selector: 'pager',
    templateUrl: './pager.component.html',
    styleUrls: ['./pager.component.css']
})
export class PagerComponent implements OnInit, OnChanges {
    @Input('paging-info')
    pagingInfo: any = null;

    @Input('paging-url')
    pagingUrl: Array<any> = [];
    
    pagingLinks: Array<any> = [];

    constructor() {
    }
    
    ngOnInit(): void {
        this.createPagingLinks();
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.createPagingLinks();
    }

    createPagingLinks() {

        this.pagingLinks = [];

        if (this.pagingInfo && this.pagingUrl && this.pagingInfo.pageCount > 1) {
            this.pagingLinks = [
                {
                    text: 'First',
                    url: this.pagingUrl,
                    query: { pageNumber: 1 },
                    show: this.pagingInfo.showFirst,
                    active: false
                },
                {
                    text: 'Prev',
                    url: this.pagingUrl,
                    query: { pageNumber: this.pagingInfo.prevPage },
                    show: this.pagingInfo.showPrev,
                    active: false
                },
                {
                    text: 'Next',
                    url: this.pagingUrl,
                    query: { pageNumber: this.pagingInfo.nextPage },
                    show: this.pagingInfo.showNext,
                    active: false
                },
                {
                    text: 'Last',
                    url: this.pagingUrl,
                    query: { pageNumber: this.pagingInfo.pageCount },
                    show: this.pagingInfo.showLast,
                    active: false
                }
            ];



            for (let i = this.pagingInfo.startPage; i <= this.pagingInfo.endPage; i++) {
                this.pagingLinks.splice(this.pagingLinks.length - 2, 0, {
                    text: i.toString(),
                    url: this.pagingUrl,
                    query: { pageNumber: i },
                    show: true,
                    active: (i == this.pagingInfo.pageNumber)
                });
            }
        }
    }
}
