import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { TokenService } from './token.service';

@Injectable()
export class SecurityService {
  constructor(private http: HttpClient, private tokenService: TokenService) { }

  registerUser(user: any) {
    return this.http.post('/api/security/register', user);
  }

  login(credentials: any) {
    return this.http.post('/api/security/authenticate', credentials);
  }

  logout() {
    this.tokenService.remove();
  }

  saveToken(token: string) {
    this.tokenService.save(token);
  }

  get isAuthenticated(): boolean {
    let authenticated = !this.tokenService.tokenExpired();

    return authenticated;
  }

  userHasRole(role: string): boolean {
    if (!this.isAuthenticated)
      return false;

    var user = this.tokenService.decode();

    return user.rls && user.rls.filter(value => value.toLowercase() === role.toLowerCase()).length;

  }

  get token(): string {
    return this.tokenService.get();
  }

  get user(): any {
    return this.tokenService.decode();
  }
}
