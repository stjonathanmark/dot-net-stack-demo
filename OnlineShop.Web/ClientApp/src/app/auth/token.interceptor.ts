import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from "rxjs";
import { SecurityService } from './security.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private securityService: SecurityService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    if (this.securityService.isAuthenticated) {
      req = req.clone({
        headers: req.headers.append('Authorization', `Bearer ${this.securityService.token}`)
      });
    }
    
    return next.handle(req)
  }
}
