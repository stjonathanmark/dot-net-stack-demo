import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { TokenService } from './token.service';
import { JwtModule, JwtHelperService } from "@auth0/angular-jwt";
import { SecurityService } from './security.service';
import { AuthGuard, AdminAuthGaurd } from './auth-guard.service';
import { TokenInterceptor } from './token.interceptor';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: new TokenService(new JwtHelperService()).get
      }
    })
  ],
  providers: [
    JwtHelperService,
    TokenService,
    SecurityService,
    AuthGuard,
    AdminAuthGaurd
    //{ provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }
  ]
})
export class AuthModule { }
