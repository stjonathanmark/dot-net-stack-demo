import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';

import { SecurityService } from './security.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(private router: Router, private securityService: SecurityService) { }
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.securityService.isAuthenticated) return true;

    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });

    return false;
  }


  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.securityService.isAuthenticated) return true;

    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });

    return false;
  }
}

class RoleAuthGuard implements CanActivate, CanActivateChild {

  protected role: string;
  protected router: Router; 
  protected securityService: SecurityService

  constructor() { }
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.securityService.user.user
    if (this.securityService.user.userHasRole(this.role)) return true;

    this.router.navigate(['/not-authorized']);

    return false;
  }


  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.securityService.userHasRole(this.role)) return true;

    this.router.navigate(['/not-authorized']);

    return false;
  }
}

@Injectable()
export class AdminAuthGaurd extends RoleAuthGuard {
  constructor(protected router: Router, protected securityService: SecurityService)
  {
    super();
    this.role  = "Administrator";
  }
}