import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class TokenService {

  constructor(private helper: JwtHelperService) { }

  tokenExpired(): boolean {
    let token = this.get();
    let expired = true;

    if (token) {
      expired = this.helper.isTokenExpired(token);
    }

    return expired;
  }

  decode(): any {
    let token = this.get();
    let tokenObj: any = null;

    if (token) {
      tokenObj = this.helper.decodeToken(token);
    }

    return tokenObj;
  }

  decodeUrlBase64(): any {
    let token = this.get();
    let tokenObj: any = null;

    if (token) {
      tokenObj = this.helper.urlBase64Decode(token);
    }

    return tokenObj;
  }

  save(token: string) {
    sessionStorage.setItem('token', token);
  }

  remove() {
    let token = this.get();

    if (token) {
      sessionStorage.removeItem('token');
    }
  }

  get(): string {
    let token = sessionStorage.getItem('token') || '';

    return token;
  }
}
