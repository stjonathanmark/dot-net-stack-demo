import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { CatalogModule } from './catalog/catalog.module';
import { ShoppingCartModule } from './shopping-cart/shopping-cart.module';
import { CheckOutModule } from './check-out/check-out.module';
import { CustomerAccountModule } from './customer-account/customer-account.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthModule } from './auth/auth.module';

export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        SignUpComponent
    ],
  imports: [
        AuthModule,
        BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
        CatalogModule,
        CheckOutModule,
        CommonModule,
        CustomerAccountModule,
        HttpClientModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'sign-up', component: SignUpComponent },
            { path: 'login', component: LoginComponent },
            { path: '**', redirectTo: 'home' }
        ]),
        ShoppingCartModule
    ],
    providers: [
        { provide: 'BASE_URL', useFactory: getBaseUrl }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

