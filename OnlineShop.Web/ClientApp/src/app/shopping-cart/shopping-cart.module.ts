import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ShoppingCartComponent } from './shopping-cart.component';

import { ShoppingCartService } from '../services/shopping-cart.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild([
            { path: 'shopping-cart', component: ShoppingCartComponent }
        ])
    ],
    declarations: [
        ShoppingCartComponent
    ],
    providers: [
        ShoppingCartService
    ]
})
export class ShoppingCartModule { }
