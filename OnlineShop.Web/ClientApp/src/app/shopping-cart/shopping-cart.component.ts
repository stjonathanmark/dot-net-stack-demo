import { Component, OnInit } from '@angular/core';

import { ShoppingCartService } from './../services/shopping-cart.service';

@Component({
    selector: 'shopping-cart',
    templateUrl: './shopping-cart.component.html',
    styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

    items: Array<any> = [];
    subtotal: number = 0;
    tax: number =  0;
    total: number = 0;

    constructor(private shoppingCartService: ShoppingCartService) { }

    ngOnInit(): void {
        this.items = this.getItems();
        this.getTotals();
    }

    onRemoveItem(productId: number) {
        this.shoppingCartService.removeItem(productId);
        this.items = this.getItems();
        this.getTotals();
    }

    onQuantityChange(productId: number, newQuantity: number) {
        if (!newQuantity || newQuantity == 0) { newQuantity = 1; }
        this.shoppingCartService.updateQuanity(productId, newQuantity);
        this.items = this.getItems();
        this.getTotals();
    }

    private getTotals() {
        this.total = this.shoppingCartService.calculateTotal();
        this.tax = this.shoppingCartService.calculateTax();
        this.subtotal = this.shoppingCartService.calculateSubTotal();
    }

    private getItems(): Array<any> {
        return this.shoppingCartService.getItems();
    }
}
