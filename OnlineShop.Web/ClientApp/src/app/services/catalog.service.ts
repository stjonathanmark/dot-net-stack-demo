import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CatalogService {

    constructor(private http: HttpClient) { }

    getCategories() {
        return this.http.get('/api/catalog/categories');
    }

    getCategory(categoryId: number) {
        return this.http.get('/api/catalog/categories/' + categoryId.toString());
    }

    getCategoryIdByProduct(productId: number) {
        return this.http.get(`/api/catalog/products/${productId}/category`);
    }

    getProducts(pageNumber:any = null, pageSize: number, medianPage: number) {
        let url = `/api/catalog/products${this.getQueryParams(pageNumber, pageSize, medianPage)}`
        return this.http.get(url);
    }

    getProductsByCategory(categoryId: number, pageNumber: any = null, pageSize: number, medianPage: number) {
        return this.http.get(`/api/catalog/categories/${categoryId.toString()}/products${this.getQueryParams(pageNumber, pageSize, medianPage)}`);
    }

    getProduct(productId: number) {
        return this.http.get('/api/catalog/products/' + productId.toString());
    }

    private getQueryParams(pageNumber:any = null, pageSize:number, medianPage:number): string
    {
        let queryParams = '';

        if (pageNumber) {
            queryParams += `?pageNumber=${pageNumber}&pageSize=${pageSize}&medianPage=${medianPage}`;
        }

        return queryParams;
    }
}
