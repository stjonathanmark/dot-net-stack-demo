﻿
import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class SharedDataService {

    onSharing: EventEmitter<any> = new EventEmitter<any>();

    constructor() { }

    share(name: string, data: any) {
        let sharedData = {
            dataName: name,
            data: data
        };

        this.onSharing.emit(sharedData);
    }
}
