import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ShoppingCartService } from './shopping-cart.service';

@Injectable()
export class CheckOutService {
    constructor(private http: HttpClient) { }

    getShippingOptions() {
        return this.http.get('/api/checkout/shipping/options');
    }

    makePayment(customerProfileId: number, products: Array<any>, shippingInfo: any, billingInfo: any, cost: any) {
        let method = billingInfo.paymentMethod;
        return this.http.post('/api/checkout/payment/make', {
            customerProfileId: customerProfileId,
            products: products,
            shippingAddress: shippingInfo.address,
            paymentMethodId: method.id > 0 ? billingInfo.paymentMethod.id : null,
            paymentTokenId: billingInfo.tokenId,
            creationTokenId: billingInfo.createTokenId,
            subTotal: cost.subTotal,
            shippingCost: cost.shippingCost,
            tax: cost.tax,
            total: cost.total
        });
    }

    getPaymentToken(card: any) {
        return this.http.post('/api/checkout/payment/token', card);
    }
}

