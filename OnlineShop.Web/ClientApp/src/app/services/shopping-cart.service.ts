﻿import { Injectable } from '@angular/core';

@Injectable()
export class ShoppingCartService {

    private key: string = 'shopping-cart-items';

    addItem(product: any, quantity: number) {
        let items = this.getItems();
        let alreadyAdded = false;
        let quantityUpdated = false;

        items.forEach((item, index, array) => {
            if (item.id == product.id) {
                if (item.quantity != quantity) {
                    item.quantity = quantity;
                    quantityUpdated = true;
                }
                alreadyAdded = true;
                return;
            }
        });

        if (!alreadyAdded) {
            product.quantity = quantity;
            items.push(product);
            this.save(items);
        } else if (quantityUpdated) {
            this.save(items);
        }

    }

    getItems(): Array<any> {
        let items: Array<any> = JSON.parse(sessionStorage.getItem(this.key) || '[]');;
        return items;
    }

    removeItem(productId: number) {
        let items = this.getItems();

        items.forEach((item, index, array) => {
            if (item.id == productId) {
                items.splice(index, 1);
                this.save(items);
                return;
            }
        });
    }

    updateQuanity(productId: number, newQuantity: number) {
        let items = this.getItems();

        items.forEach((item, index, array) => {
            if (item.id == productId) {
                if (item.quantity != newQuantity) {
                    item.quantity = newQuantity;
                    this.save(items);
                }
                return;
            }
        });
    }

    clearItems() {
        sessionStorage.removeItem(this.key);
    }

    calculateTotal(): number {
        let subtotal = this.calculateSubTotal();
        let tax = this.calculateTax();

        let total = subtotal + tax;

        return total;
    }

    calculateTax(): number {
        let subtotal = this.calculateSubTotal();

        let calculatedTax = 0;

        if (subtotal > 0) {
            calculatedTax = (subtotal / 100) * 7;
        }

        return calculatedTax;
    }

    calculateSubTotal(): number {
        let items = this.getItems();

        let calculatedSubtotal: number = 0;

        if (items && items.length) {
            items.forEach((item, index, array) => {
                calculatedSubtotal += item.price * item.quantity;
            });
        }

        return calculatedSubtotal;
    }

    private save(items: Array<any>) {
        sessionStorage.setItem(this.key, JSON.stringify(items));
    }
}
