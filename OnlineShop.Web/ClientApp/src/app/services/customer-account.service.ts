import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CustomerAccountService {

  constructor(private http: HttpClient) { }

    createUser(user: any) {
        this.http.post('/account/createuser', user);
    }

    authenticate(credentials: any) {
        return this.http.post('/account/authenticate', credentials);
    }

    saveToken(token: string) {
        localStorage.setItem('token', token);
    }

    getToken(): string {
        let token: string = localStorage.getItem('token') || '';
        return token;
    }
}
