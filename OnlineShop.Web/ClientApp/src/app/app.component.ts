import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SecurityService } from './auth/security.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    loggedIn: boolean = false;

    constructor(private router: Router, private securityService: SecurityService) { }

    ngOnInit() {
        this.checkAuthenticationStatus();
    }

    onActivated(component: Component) {
        this.checkAuthenticationStatus();
    }

    logout() {
        this.securityService.logout();
        this.checkAuthenticationStatus();
        return false;
    }

    checkAuthenticationStatus() {
        this.loggedIn = this.securityService.isAuthenticated;
    }
}
