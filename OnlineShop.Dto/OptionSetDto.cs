﻿using System.Collections.Generic;

namespace OnlineShop.Dto
{
    public class OptionSetDto : BaseOptionSet
    {
        public OptionSetDto()
        {
            Options = new List<OptionDto>();
        }

        public ProductDto Product { get; set; }

        public List<OptionDto> Options { get; set; }
    }
}
