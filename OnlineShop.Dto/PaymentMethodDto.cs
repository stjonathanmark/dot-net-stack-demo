﻿
using System.Collections.Generic;

namespace OnlineShop.Dto
{
    public class PaymentMethodDto : BasePaymentMethod
    {
        public PaymentMethodDto()
        {
            Orders = new List<OrderDto>();
        }
        public CustomerProfileDto CustomerProfile { get; set; }

        public List<OrderDto> Orders { get; set; }

        public string FormattedExpirationDate { get; set; }

        public bool IsExpired { get; set; }
    }
}
