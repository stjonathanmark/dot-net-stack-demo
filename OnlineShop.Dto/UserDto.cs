﻿using System.Collections.Generic;

namespace OnlineShop.Dto
{
    public class UserDto : BaseUser
    {
        public UserDto()
        {
            UserRoles = new List<UserRoleDto>();
            EmailAddresses = new List<EmailAddressDto>();
            Passwords = new List<PasswordDto>();
        }

        public CustomerProfileDto CustomerProfile { get; set; }

        public List<UserRoleDto> UserRoles { get; set; }

        public List<EmailAddressDto> EmailAddresses { get; set; }

        public List<PasswordDto> Passwords { get; set; }
    }
}
