﻿
namespace OnlineShop.Dto
{
    public class TokenRoleDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
