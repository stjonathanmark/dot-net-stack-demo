﻿using System.Collections.Generic;

namespace OnlineShop.Dto
{
    public class ProductDto : BaseProduct
    {
        public ProductDto()
        {
            OptionSets = new List<OptionSetDto>();
            OrderedItems = new List<OrderedItemDto>();
            ChosenOptions = new List<int>();
        }

        public CategoryDto Category { get; set; }

        public List<OptionSetDto> OptionSets { get; set; }

        public List<OrderedItemDto> OrderedItems { get; set; }

        public int Quantity { get; set; }

        public List<int> ChosenOptions { get; set; }
    }
}
