﻿
namespace OnlineShop.Dto
{
    public class PaymentTokenDto : BasePaymentToken
    {
        public PaymentMethodDto PaymentMethod { get; set; }
    }
}
