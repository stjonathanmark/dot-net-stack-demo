﻿
namespace OnlineShop.Dto
{
    public class PasswordDto : BasePassword
    {
        public UserDto User { get; set; }
    }
}
