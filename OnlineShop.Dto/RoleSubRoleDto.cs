﻿
namespace OnlineShop.Dto
{
    public class RoleSubRoleDto : BaseRoleSubRole
    {
        public RoleDto Role { get; set; }

        public RoleDto SubRole { get; set; }
    }
}
