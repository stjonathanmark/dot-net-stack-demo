﻿
namespace OnlineShop.Dto
{
    public class PhoneNumberDto : BasePhoneNumber
    {
        public CustomerProfileDto CustomerProfile { get; set; }
    }
}
