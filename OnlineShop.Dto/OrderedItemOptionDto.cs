﻿

namespace OnlineShop.Dto
{
    public class OrderedItemOptionDto : BaseOrderedItemOption
    {
        public OrderedItemDto OrderedItem { get; set; }

        public OptionDto Option { get; set; }
    }
}
