﻿using System.Collections.Generic;

namespace OnlineShop.Dto
{
    public class RoleDto : BaseRole
    {
        public RoleDto()
        {
            UserRoles = new List<UserRoleDto>();
            RoleSubRoles = new List<RoleSubRoleDto>();
        }

        public List<UserRoleDto> UserRoles { get; set; }

        public List<RoleSubRoleDto> RoleSubRoles { get; set; }
    }
}
