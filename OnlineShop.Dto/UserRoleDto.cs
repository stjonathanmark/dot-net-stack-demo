﻿
namespace OnlineShop.Dto
{
    public class UserRoleDto : BaseUserRole
    {
        public UserDto User { get; set; }

        public RoleDto Role { get; set; }
    }
}
