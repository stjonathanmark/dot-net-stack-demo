﻿using System.Collections.Generic;

namespace OnlineShop.Dto
{
    public class StateDto : BaseState
    {
        public StateDto()
        {
            Addresses = new List<AddressDto>();
        }

        public CountryDto Country { get; set; }

        public List<AddressDto> Addresses { get; set; }
    }
}
