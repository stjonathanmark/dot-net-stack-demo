﻿using System.Collections.Generic;

namespace OnlineShop.Dto
{
    public class CategoryDto : BaseCategory
    {
        public CategoryDto()
        {
            SubCategories = new List<CategoryDto>();
            Products = new List<ProductDto>();
        }

        public CategoryDto ParentCategory { get; set; }

        public List<CategoryDto> SubCategories { get; set; }

        public List<ProductDto> Products { get; set; }
    }
}
