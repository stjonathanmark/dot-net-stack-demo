﻿
namespace OnlineShop.Dto
{
    public class BillingInformationDto : BaseBillingInformation
    {
        public BillingInformationDto()
        {
            Card = new CardDto();
        }

        CardDto Card { get; set; }
    }
}
