﻿
namespace OnlineShop.Dto
{
    public class CardDto : BaseCard
    {
        public CardDto()
        {
            Address = new BillingAddressDto();
        }

        public BillingAddressDto Address { get; set; }
    }
}
