﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShop.Dto
{
    public class EmailAddressDto : BaseEmailAddress
    {
        public UserDto User { get; set; }

        public CustomerProfileDto CustomerProfile { get; set; }
    }
}
