﻿using System.Collections.Generic;

namespace OnlineShop.Dto
{
    public class CustomerProfileDto : BaseCustomerProfile
    {
        public CustomerProfileDto()
        {
            Addresses = new List<AddressDto>();
            EmailAddresses = new List<EmailAddressDto>();
            PhoneNumbers = new List<PhoneNumberDto>();
            PaymentMethods = new List<PaymentMethodDto>();
            Orders = new List<OrderDto>();
        }

        public UserDto User { get; set; }

        public List<AddressDto> Addresses { get; set; }

        public List<EmailAddressDto> EmailAddresses { get; set; }

        public List<PhoneNumberDto> PhoneNumbers { get; set; }

        public List<PaymentMethodDto> PaymentMethods { get; set; }

        public List<OrderDto> Orders { get; set; }
    }
}
