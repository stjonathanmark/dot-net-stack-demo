﻿using System.Collections.Generic;

namespace OnlineShop.Dto
{
    public class OptionDto : BaseOption
    {
        public OptionDto()
        {
            OrderedItemOptions = new List<OrderedItemOptionDto>();
        }
        public OptionSetDto OptionSet { get; set; }

        public List<OrderedItemOptionDto> OrderedItemOptions { get; set; }
    }
}
