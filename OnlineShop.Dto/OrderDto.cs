﻿using System.Collections.Generic;

namespace OnlineShop.Dto
{
    public class OrderDto : BaseOrder
    {
        public OrderDto()
        {
            OrderedItems = new List<OrderedItemDto>();
        }

        public CustomerProfileDto CustomerProfile { get; set; }

        public List<OrderedItemDto> OrderedItems { get; set; }

        public AddressDto ShippingAddress { get; set; }

        public PaymentMethodDto PaymentMethod { get; set; }
    }
}
