﻿using System.Collections.Generic;
using System.Security.Claims;

namespace OnlineShop.Dto
{
    public class TokenUserDto
    {
        public TokenUserDto()
        {
            Roles = new List<TokenRoleDto>();
        }

        public long UserId { get; set; }

        public long? CustomerId { get; set; }

        public string Username { get; set; }

        public string FullName { get; set; }

        public string CompanyName { get; set; }

        public List<TokenRoleDto> Roles { get; set; }
    }
}
