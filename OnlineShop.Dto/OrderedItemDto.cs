﻿using System.Collections.Generic;

namespace OnlineShop.Dto
{
    public class OrderedItemDto : BaseOrderedItem
    {
        public OrderedItemDto()
        {
            OrderedItemOptions = new List<OrderedItemOptionDto>();
        }

        public OrderDto Order { get; set; }

        public ProductDto Product { get; set; }

        public List<OrderedItemOptionDto> OrderedItemOptions { get; set; }
    }
}
