﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShop.Dto
{
    public class AddressDto : BaseAddress
    {
        public CustomerProfileDto CustomerProfile { get; set; }

        public StateDto State { get; set; }

        public CountryDto Country { get; set; }
    }
}
