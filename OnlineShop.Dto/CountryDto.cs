﻿using System.Collections.Generic;

namespace OnlineShop.Dto
{
    public class CountryDto
    {
        public CountryDto()
        {
            States = new List<StateDto>();
            Addresses = new List<AddressDto>();
        }

        public List<StateDto> States { get; set; }

        public List<AddressDto> Addresses { get; set; }
    }
}
